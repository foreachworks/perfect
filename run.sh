#!/usr/bin/bash

echo '======================='
echo '>>> Start Install'
echo '======================='

composer install
php artisan key:generate
php artisan migrate:fresh


echo '======================='
echo '>>> Complete Install'
echo '======================='
