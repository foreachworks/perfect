<?php

use Illuminate\Database\Seeder;

class WalletStatusTableSeeder extends Seeder
{
    public function run()
    {
		DB::table('wallet_status')->insert([
			'business_account' => 0,
			'verified_account' => 0,
			'trust_score' => 0
		]);        
    }
}
