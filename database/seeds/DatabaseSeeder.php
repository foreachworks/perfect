<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
/*		
        $this->call(UsersTableSeeder::class);
        $this->call(LogTypeTableSeeder::class);
        $this->call(LogActionTableSeeder::class);
        $this->call(WalletTypeTableSeeder::class);
        $this->call(WalletTableSeeder::class);
		
        $this->call(WalletStatusTableSeeder::class);
        $this->call(CabinetBannersTableSeeder::class);
*/		
        $this->call(WrnMessageTableSeeder::class);
    }
}
