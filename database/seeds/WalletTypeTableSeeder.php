<?php

use App\WalletType;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: illidan
 * Date: 18.12.2018
 * Time: 21:07
 */

class WalletTypeTableSeeder extends Seeder
{

    public function run()
    {
        $type = [
            'Troy oz.',
            'USD',
            'EUR'
        ];

        foreach ($type as $item){
            WalletType::create([
                'title' => $item
            ]);
        }
    }
}