<?php

use App\LogAction;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: illidan
 * Date: 18.12.2018
 * Time: 21:07
 */

class LogActionTableSeeder  extends Seeder
{

    public function run()
    {
        $type = [
            'Change',
            'Create',
            'Make',
            'Cancel',
            'Send',
            'Complete',
            'Discard',
            'Add',
            'Receive',
            'Login',
            'Logout',
            'Exchange',
            'Transfer',
        ];

        foreach ($type as $item){
            LogAction::create([
                'title' => $item
            ]);
        }
    }
}