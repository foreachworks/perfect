<?php

use Illuminate\Database\Seeder;

class CabinetBannersTableSeeder extends Seeder
{
    public function run()
    {
		DB::table('cabinet_banners')->insert([
			'img1' => '/img/rand/top2-70.png',
			'img2' => '/img/rand/mid3-70.png'
		]);        
			
		DB::table('cabinet_banners')->insert([
			'img1' => '/img/rand/confidentiality.jpg',
			'img2' => '/img/rand/text-for-confidentiality.png'
		]);        
			
		DB::table('cabinet_banners')->insert([
			'img1' => '/img/rand/speed.jpg',
			'img2' => '/img/rand/Text-for-speed.png'
		]);        
    }
}
