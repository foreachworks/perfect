<?php

use App\LogType;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: illidan
 * Date: 18.12.2018
 * Time: 21:07
 */

class LogTypeTableSeeder  extends Seeder
{

    public function run()
    {
        $type = [
            'User',
            'Account',
            'Deposit',
            'Withdrawal',
            'Mail',
            'Security',
            'Exchange',
            'SMS',
            'Public',
            'Reccurent Payment',
        ];

        foreach ($type as $item){
            LogType::create([
                'title' => $item
            ]);
        }
    }
}