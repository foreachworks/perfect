<?php

use App\Wallet;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: illidan
 * Date: 18.12.2018
 * Time: 21:07
 */

class WalletTableSeeder extends Seeder
{

    public function run()
    {
        factory(Wallet::class, 5)->create();
    }
}