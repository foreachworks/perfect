<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWrnMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wrn_message', function (Blueprint $table) {
            $table->increments('id');
			$table->smallInteger('wrn')->nullable()->default(0);
            $table->timestamps();
        });
		
		DB::table('wrn_message')->insert([
			'wrn' => 0
		]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wrn_message');
    }
}
