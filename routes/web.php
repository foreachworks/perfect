<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('/my-logout', 'yoursessioncontroller@logout')->name('my.logout');

Route::get('/', 'HomeController@home')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile.html', 'HomeController@index')->name('cabinet');
    Route::get('/history.html/{paginate?}', 'HomeController@history')->name('history');
    Route::get('/business.html', 'HomeController@businessAccount')->name('business');
    Route::post('/send_preview.html', 'HomeController@payment')->name('payment');
    Route::post('/send_money.html', 'HomeController@editPayment')->name('edit');
    Route::get('/send_money/{segment2}', 'HomeController@sendInternPayment')->name('send_intern');
    Route::post('/result.html', 'HomeController@runPayment')->name('run.payment');
    Route::get('/security.html', 'HomeController@security')->name('security');
    Route::post('/change_security', 'HomeController@change_security');
    Route::get('/send.html', 'HomeController@internPayment')->name('intern');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('/account', 'DashboardController@account')->name('account');
        Route::get('/info-edit/{user}', 'DashboardController@editInfo')->name('info.edit');
        Route::post('/update-info', 'DashboardController@updateInfo')->name('update.info');
        Route::resource('wallet', 'DashboardController');
        Route::resource('log', 'LogController');
        Route::get('create-range', 'LogController@createRange')->name('log.create.range');
        Route::post('store-range', 'LogController@storeRange')->name('store.range');
        Route::get('clear-all-history', 'LogController@clear')->name('log.clear.all');
    });
	
	Route::post('/edit-wrn', 'DashboardController@updateWrn');		
	
});