// глобальные переменные
var container, camera, controls, scene, renderer, light;
var Cube;
// начинаем рисовать после полной загрузки страницы
window.onload = function()
{
//	init();
//	animate();

	scene = new THREE.Scene(); //создаем сцену

	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setClearColor(0xffffff);
	renderer.setSize(window.innerWidth, window.innerHeight);

	container = document.getElementById('MyWebGLApp');
	container.appendChild(renderer.domElement);

	AddCamera(0, 300, 500); // добавляем камеру
	AddLight(0, 0, 500); //устанавливаем белый свет

	var geometry = new THREE.BoxGeometry(200, 100, 150);
	var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
	Cube = new THREE.Mesh(geometry, material);
	Cube.position.z = -100;
	Cube.rotation.z = Math.PI / 6;
	scene.add(Cube);

	controls.update();
	renderer.render(scene, camera);
	
}

function init()
{
	scene = new THREE.Scene(); //создаем сцену
	//создаем рендерер
	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setClearColor(0xffffff);
	renderer.setSize(window.innerWidth, window.innerHeight);

	container = document.getElementById('MyWebGLApp');
	container.appendChild(renderer.domElement);
	AddCamera(0, 300, 500); // добавляем камеру
	AddLight(0, 0, 500); //устанавливаем белый свет


	//добавляем куб
	var geometry = new THREE.BoxGeometry(200, 100, 150);
	var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
	Cube = new THREE.Mesh(geometry, material);
	Cube.position.z = -100;
	Cube.rotation.z = Math.PI / 6;
	scene.add(Cube);
	
	renderer.render(scene, camera);

}

function animate()
{
//	requestAnimationFrame(animate);
	render();
}

function render()
{
	Cube.position.x = Cube.position.x + 1; //куб движется
	Cube.rotation.y = Cube.rotation.y + 0.01; //и вращается вокруг оси
	controls.update();
	renderer.render(scene, camera);
}

function AddCamera(X,Y,Z)
{
	camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight, 1, 10000);
	camera.position.set(X,Y,Z);
	controls = new THREE.TrackballControls(camera, container);
	controls.rotateSpeed = 2;
	controls.noZoom = false;
	controls.zoomSpeed = 1.2;
	controls.staticMoving = true;
}

function AddLight(X,Y,Z)
{
	light = new THREE.DirectionalLight(0xffffff);
	light.position.set(X,Y,Z);
	scene.add(light);
}
