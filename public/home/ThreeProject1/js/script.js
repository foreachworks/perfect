function init(geo)
{	
	
	var element = document.getElementById('MyWebGLApp');


	var scene  = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight);
	camera.position.z = 100;
	camera.position.x = 50;

	var controls = new THREE.TrackballControls(camera, element);

	var renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setClearColor(0xDDDDDD, 1);
	renderer.setSize(window.innerWidth, window.innerHeight);
	element.appendChild(renderer.domElement);

	if(geo == 1)
	{
		var geometry = new THREE.BoxGeometry(1, 1, 30);
	}
	else
	{
		var geometry = new THREE.CylinderGeometry( 5, 5, 70, 32 );
	}


//	var geometry = new THREE.BoxGeometry(1, 1, 30);
//	var geometry = new THREE.CylinderGeometry( 5, 5, 70, 32 );
	var material = new THREE.MeshPhongMaterial({ color: 0x0095DD });
	var mesh = new THREE.Mesh(geometry, material);

	var pointLight = new THREE.PointLight(0xFFFFFF);
	pointLight.position.set(-10, 15, 50);

	scene.add(camera);
	scene.add(mesh);
	scene.add(pointLight);
	
	function resize() {
		var width = element.clientWidth;
		var height = element.clientHeight;

		renderer.setSize(width, height);
		camera.aspect = width/height;
		controls.handleResize();
	}
		
	function animate() {
		requestAnimationFrame(animate);    
	//    mesh.rotation.y += 0.1 * Math.PI/180;
		controls.update();
		renderer.render(scene, camera);
	}

	resize();
	animate();	

}
/*
	function resize() {
		var width = element.clientWidth;
		var height = element.clientHeight;

		renderer.setSize(width, height);
		camera.aspect = width/height;
		controls.handleResize();
	}

	function animate() {
		requestAnimationFrame(animate);    
	//    mesh.rotation.y += 0.1 * Math.PI/180;
		controls.update();
		renderer.render(scene, camera);
	}
*/
//	init();
//	resize();
//	animate();	

