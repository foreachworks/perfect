@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

    <table width="100%" class="qwer" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26"></td>
            <td width="94%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr background="/public/cabinet/bgg.gif">
                        <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        @include('nav_menu.to_date')
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img src="/public/cabinet/red.gif" width="100%"
                                                                    height="1"></td>
                                </tr>
                                </tbody>
                            </table>
                            <div id="quick_wrapper" style="background-color:#fff;min-height:60px;display:none"><br><img
                                        src="/public/cabinet/loader.gif"></div>
                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', '62fecd7cbe2164daef1be80f04fa71f6', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>


                            <br>


                            <table width="750" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td width="23" valign="top"><img src="/public/cabinet/tlefttop.gif" width="23"
                                                                     height="42"></td>
                                    <td background="/public/cabinet/tbg.gif" valign="bottom" nowrap="">
                                        <font color="004E97" face="Arial, Helvetica, sans-serif" size="5"><img
                                                    src="/public/cabinet/blank.gif" width="15" height="10">
                                            Внутренний перевод
                                        </font>
                                    </td>
                                    <td background="/public/cabinet/trighttop.gif" width="999"><img
                                                src="/public/cabinet/tcenter.gif" width="60" height="42"></td>
                                    <td valign="bottom" width="10"><img src="/public/cabinet/trr.gif" width="10"
                                                                        height="42"></td>
                                </tr>
                                <tr>
                                    <td background="/public/cabinet/tleft.gif">&nbsp;</td>
                                    <td colspan="2" bgcolor="#FFFFFF">
                                        <p>&nbsp;</p>

                                        <div style="padding: 0px 40px 0px 18px">
                                            <div class="arabic">
                                                <font size="4">Ваш баланс {{' ' . $data['balance'] . ' ' . $data['type_wallet'] . ', ' . $data['number']}}</font><br><br>
                                            </div>

                                            <div class="arabic" id="send">
                                                <div align="right"><img src="/public/cabinet/lock4.jpg">&nbsp;&nbsp;Безопасный
                                                    Перевод&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                Вы можете отправить деньги любому пользователю Perfect Money.<p>Пожалуйста,
                                                    заполните форму для того, чтобы осуществить перевод.</p>
                                                <br><br>

                                                <div style="margin-right: 10px;" class="box3">
                                                    <form method="post" action="{{route('payment')}}">
                                                        @csrf
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120"></td>
                                                                <td width="490"></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120">Сумма: <font size="2"
                                                                                                                    color="#CC0000">*</font>
                                                                </td>
                                                                <td width="490">
                                                                    <input type="text" name="amount" size="20"
                                                                           maxlength="20" value="{{$data['amount']}}" autocomplete="off">{{$data['type_wallet']}}
                                                                    <div class="error" id="balance_error"></div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td valign="top" nowrap="" width="120">На аккаунт: <font
                                                                            size="2" color="#CC0000">*</font></td>
                                                                <td width="490">
                                                                    <input style="float: left" type="text" name="to_wallet" size="20" maxlength="20" value="{{$data['to_wallet']}}" autocomplete="off">
                                                                    <a href="#"><img
                                                                                src="/public/cabinet/payee_list.png"
                                                                                title="Показать список получателей"
                                                                                style="border:0;vertical-align:text-bottom;"></a>
                                                                    <div id="payees" class="">
                                                                        <br>
                                                                        Ваш список получателей пуст. Вы можете добавить
                                                                        нового пользователя, поставив галочку рядом, или
                                                                        кликнув <a
                                                                                href="#">эту
                                                                            ссылку</a>.
                                                                    </div>
                                                                    <script type="text/javascript"
                                                                            src="/public/cabinet/jquery.dataTables.js.завантаження"></script>
                                                                    <script type="text/javascript">
                                                                        $j(document).ready(function () {
                                                                            $j("#payees_list").dataTable({
                                                                                "iDefaultSortIndex": 2,
                                                                                "sDefaultSortDirection": "asc"
                                                                            });
                                                                        });
                                                                    </script>

                                                                    <div class="error"></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120">Описание:</td>
                                                                <td width="490">
                                                                <textarea name="memo"
                                                                          cols="60" rows="3">{{isset($data['memo']) ? $data['memo'] : ''}}</textarea>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120"></td>
                                                                <td width="490">
                                                                    <input type="checkbox" >
                                                                    Добавить Получателя в мой список
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120"></td>
                                                                <td width="490">
                                                                    <input type="checkbox" id="protection"
                                                                           onclick="check()">
                                                                    Добавить код протекции
                                                                </td>
                                                            </tr>
                                                            <tr id="pdata3" style="display: none;">
                                                                <td valign="top" colspan="2"><b><br>
                                                                        Важно</b> <br>
                                                                    <p class="txt">Если данная опция активирована, то
                                                                        Получатель перевода должен ввести код протекции для
                                                                        получения Вашего перевода. В случае, если Получатель
                                                                        не введет правильный код протекции, платеж
                                                                        автоматически отправляется отправителю по истечении
                                                                        периода действия кода протекции. Перевод не может
                                                                        быть отменен до окончания периода действия кода
                                                                        протекции.</p>
                                                                    <br>
                                                                </td>
                                                            </tr>
                                                            <tr id="pdata2" style="display: none;">
                                                                <td valign="top" nowrap="" width="120">Код протекции<br>
                                                                    <small><font color="#999999">Макс. 20 символов</font>
                                                                    </small>
                                                                </td>
                                                                <td width="490" nowrap="">
                                                                    <input type="text" name="code" id="code" size="20"
                                                                           maxlength="19" value="" disabled="">
                                                                    <input type="checkbox" id="auto"
                                                                           onclick="check2()" checked="">
                                                                    Отметить для генерации случайного кода
                                                                    <div class="error"></div>
                                                                    <img src="/public/cabinet/blank.gif" width="14"
                                                                         height="16"><br>
                                                                </td>
                                                            </tr>
                                                            <tr id="pdata" style="display: none;">
                                                                <td valign="top" nowrap="" width="120">Период действия кода
                                                                    протекции<br>
                                                                    <small><font color="#999999">1-365 дней</font></small>
                                                                </td>
                                                                <td width="490" nowrap="">
                                                                    <input type="text" id="period" size="4"
                                                                           maxlength="3" value="">
                                                                    <div class="error"></div>
                                                                    <img src="/public/cabinet/blank.gif" width="14"
                                                                         height="16"><br>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120">&nbsp;</td>
                                                                <td width="490">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" nowrap="" width="120">&nbsp;</td>
                                                                <td width="490">
                                                                    <input type="submit" value="Предпросмотр платежа"
                                                                           class="button" id="sbt">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                    </form>
                                                </div>
                                            </div>
                                            <table width="200" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                <tr>
                                                    <td width="1"><a href="javascript:history.go(-1)"><img
                                                                    src="/public/cabinet/bck.gif" border="0"></a></td>
                                                    <td>Предыдущая страница</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <script language="JavaScript">

                                                function check() {
                                                    if ($j('#protection').is(':checked')) {
                                                        $j('#pdata').show();
                                                        $j('#pdata2').show();
                                                        $j('#pdata3').show();
                                                    } else {
                                                        $j('#pdata').hide();
                                                        $j('#pdata2').hide()
                                                        $j('#pdata3').hide()
                                                    }
                                                    ;
                                                }

                                                function check2() {
                                                    if ($j('#auto').is(':checked')) $j('#code').attr('disabled', 'disabled'); else $j('#code').attr('disabled', '')
                                                }

                                                check();
                                                check2();

                                                $j('#send_form').submit(function (e) {
                                                    $j.cookie('send_cookie', '2bc77280a11b2fff188e64ecf49bdfd2', {path: '/'});
                                                    return true;
                                                });
                                            </script>

                                        </div>
                                    </td>
                                    <td background="/public/cabinet/tright.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right"><img src="/public/cabinet/bottomleft.gif"></div>
                                    </td>
                                    <td background="/public/cabinet/bottom.gif" colspan="2">
                                        <div align="center"><img src="/public/cabinet/bottomleft.gif" width="1"
                                                                 height="1"></div>
                                    </td>
                                    <td><img src="/public/cabinet/rightbottom.gif" width="10" height="17"></td>
                                </tr>
                                </tbody>
                            </table>


                            <br>


                        </td>
                    </tr>

                    <tr valign="middle">
                        <td bgcolor="#FFFFFF" valign="middle" height="12"><img src="/public/cabinet/red.gif"
                                                                               width="100%" height="1"></td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24" height="26"></td>
        </tr>
        </tbody>
    </table>

    <!--FOOTER-->

    @include('nav_menu.footer')

@endsection