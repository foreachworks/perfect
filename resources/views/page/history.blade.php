@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

    <table width="100%" class="qwer" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26"></td>
            <td width="94%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr background="/public/cabinet/bgg.gif">
                        <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">
                                        <span id="actions"></span>
                                        <script type="text/javascript">
                                            function getUserEvents() {
                                                $j.get('/user/events.asp?section=history_main', null, function (data) {
                                                    if (data && data.length > 1) {
                                                        var params = JSON.parse(data);
                                                        $j('#actions').html(params.actions);
                                                        $j('#messages').html(params.news);
                                                    }
                                                });
                                                setTimeout('getUserEvents()', 100000);
                                            }

                                            setTimeout('getUserEvents()', 1500);
                                        </script>
                                        <noscript>
                                            <img src="/img/attention.gif" height="16" widht="16"> <font color="red"><b>Please,
                                                    enable Javascript for this site</b></font>
                                        </noscript>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        @include('nav_menu.to_date')
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img src="/public/cabinet/red.gif" width="100%"
                                                                    height="1">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            @include('nav_menu.payment')
                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', 'd8384dc6fd5d9010c43f88ce04bff565', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>


                            <br>


                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td width="23" valign="top"><img src="/public/cabinet/tlefttop.gif" width="23"
                                                                     height="42"></td>
                                    <td background="/public/cabinet/tbg.gif" valign="bottom" nowrap="">
                                        <font color="004E97" face="Arial, Helvetica, sans-serif" size="5"><img
                                                    src="/public/cabinet/blank.gif" width="15" height="10">
                                            История аккаунта
                                        </font>
                                    </td>
                                    <td background="/public/cabinet/trighttop.gif" width="999"><img
                                                src="/public/cabinet/tcenter.gif" width="60" height="42"></td>
                                    <td valign="bottom" width="10"><img src="/public/cabinet/trr.gif" width="10"
                                                                        height="42"></td>
                                </tr>
                                <tr>
                                    <td background="/public/cabinet/tleft.gif">&nbsp;</td>
                                    <td colspan="2" bgcolor="#FFFFFF">
                                        <p>&nbsp;</p>

                                        <div style="padding: 0px 40px 0px 20px">
                                            <br>
                                            <script language="JavaScript"
                                                    src="/public/cabinet/CalendarPopup.js.завантаження"></script>
                                            <script language="JavaScript">document.write(getCalendarStyles());</script>
                                            <style>
                                                .cpYearNavigation, .cpMonthNavigation {
                                                    background-color: #C0C0C0;
                                                    text-align: center;
                                                    vertical-align: center;
                                                    text-decoration: none;
                                                    color: #000000;
                                                    font-weight: bold;
                                                }

                                                .cpDayColumnHeader, .cpYearNavigation, .cpMonthNavigation, .cpCurrentMonthDate, .cpCurrentMonthDateDisabled, .cpOtherMonthDate, .cpOtherMonthDateDisabled, .cpCurrentDate, .cpCurrentDateDisabled, .cpTodayText, .cpTodayTextDisabled, .cpText {
                                                    font-family: arial;
                                                    font-size: 8pt;
                                                }

                                                TD.cpDayColumnHeader {
                                                    text-align: right;
                                                    border: solid thin #C0C0C0;
                                                    border-width: 0px 0px 1px 0px;
                                                }

                                                .cpCurrentMonthDate, .cpOtherMonthDate, .cpCurrentDate {
                                                    text-align: right;
                                                    text-decoration: none;
                                                }

                                                .cpCurrentMonthDateDisabled, .cpOtherMonthDateDisabled, .cpCurrentDateDisabled {
                                                    color: #D0D0D0;
                                                    text-align: right;
                                                    text-decoration: line-through;
                                                }

                                                .cpCurrentMonthDate, .cpCurrentDate {
                                                    color: #000000;
                                                }

                                                .cpOtherMonthDate {
                                                    color: #808080;
                                                }

                                                TD.cpCurrentDate {
                                                    color: white;
                                                    background-color: #C0C0C0;
                                                    border-width: 1px;
                                                    border: solid thin #800000;
                                                }

                                                TD.cpCurrentDateDisabled {
                                                    border-width: 1px;
                                                    border: solid thin #FFAAAA;
                                                }

                                                TD.cpTodayText, TD.cpTodayTextDisabled {
                                                    border: solid thin #C0C0C0;
                                                    border-width: 1px 0px 0px 0px;
                                                }

                                                A.cpTodayText, SPAN.cpTodayTextDisabled {
                                                    height: 20px;
                                                }

                                                A.cpTodayText {
                                                    color: black;
                                                }

                                                .cpTodayTextDisabled {
                                                    color: #D0D0D0;
                                                }

                                                .cpBorder {
                                                    border: solid thin #808080;
                                                }
                                            </style>

                                            <script language="JavaScript">
                                                var cal = new CalendarPopup("dates");
                                                cal.showNavigationDropdowns();
                                            </script>
                                            <div class="arabic">
                                                <p>Вы можете просмотреть все записи транзакций, совершенных на Вашем
                                                    аккаунте.</p>
                                                <p><a href="#">Нажмите сюда</a> для
                                                    просмотра подробной информации о Ваших реферралах. </p>
                                                <table border="0" width="98%" cellpadding="5" cellspacing="5">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <script language="JavaScript"
                                                                    src="/public/cabinet/CalendarPopup.js.завантаження"></script>
                                                            <script language="JavaScript">document.write(getCalendarStyles());</script>
                                                            <style>
                                                                .cpYearNavigation, .cpMonthNavigation {
                                                                    background-color: #C0C0C0;
                                                                    text-align: center;
                                                                    vertical-align: center;
                                                                    text-decoration: none;
                                                                    color: #000000;
                                                                    font-weight: bold;
                                                                }

                                                                .cpDayColumnHeader, .cpYearNavigation, .cpMonthNavigation, .cpCurrentMonthDate, .cpCurrentMonthDateDisabled, .cpOtherMonthDate, .cpOtherMonthDateDisabled, .cpCurrentDate, .cpCurrentDateDisabled, .cpTodayText, .cpTodayTextDisabled, .cpText {
                                                                    font-family: arial;
                                                                    font-size: 8pt;
                                                                }

                                                                TD.cpDayColumnHeader {
                                                                    text-align: right;
                                                                    border: solid thin #C0C0C0;
                                                                    border-width: 0px 0px 1px 0px;
                                                                }

                                                                .cpCurrentMonthDate, .cpOtherMonthDate, .cpCurrentDate {
                                                                    text-align: right;
                                                                    text-decoration: none;
                                                                }

                                                                .cpCurrentMonthDateDisabled, .cpOtherMonthDateDisabled, .cpCurrentDateDisabled {
                                                                    color: #D0D0D0;
                                                                    text-align: right;
                                                                    text-decoration: line-through;
                                                                }

                                                                .cpCurrentMonthDate, .cpCurrentDate {
                                                                    color: #000000;
                                                                }

                                                                .cpOtherMonthDate {
                                                                    color: #808080;
                                                                }

                                                                TD.cpCurrentDate {
                                                                    color: white;
                                                                    background-color: #C0C0C0;
                                                                    border-width: 1px;
                                                                    border: solid thin #800000;
                                                                }

                                                                TD.cpCurrentDateDisabled {
                                                                    border-width: 1px;
                                                                    border: solid thin #FFAAAA;
                                                                }

                                                                TD.cpTodayText, TD.cpTodayTextDisabled {
                                                                    border: solid thin #C0C0C0;
                                                                    border-width: 1px 0px 0px 0px;
                                                                }

                                                                A.cpTodayText, SPAN.cpTodayTextDisabled {
                                                                    height: 20px;
                                                                }

                                                                A.cpTodayText {
                                                                    color: black;
                                                                }

                                                                .cpTodayTextDisabled {
                                                                    color: #D0D0D0;
                                                                }

                                                                .cpBorder {
                                                                    border: solid thin #808080;
                                                                }
                                                            </style>

                                                            <script language="JavaScript">
                                                                var cal = new CalendarPopup("dates");
                                                                cal.showNavigationDropdowns();
                                                            </script>
                                                            <form action="#"
                                                                  method="post" name="filter">
                                                                <table border="0" class="filter_off" cellpadding="0"
                                                                       cellspacing="5" width="auto">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="right" nowrap="">От</td>
                                                                        <td width="120">
                                                                            <input type="text" value="12/01/2018"
                                                                                   name="created1" default="12/01/2018"
                                                                                   size="10"
                                                                                   style="float: left; margin-right: 5px;">
                                                                            <a href="#"
                                                                               name="anchor1" id="anchor1">
                                                                                <img src="/public/cabinet/calendar.jpg"
                                                                                     alt="" width="16" height="15"
                                                                                     border="0">
                                                                            </a>
                                                                        </td>
                                                                        <td width="80" align="right">Сумма от</td>
                                                                        <td><input type="text" value="" name="amount1"
                                                                                   size="10"></td>
                                                                        <td width="80" align="right">Объект</td>
                                                                        <td><select name="object">
                                                                                <option></option>
                                                                                <option value="1">User</option>
                                                                                <option value="2">Account</option>
                                                                                <option value="3">Deposit</option>
                                                                                <option value="7">Withdrawal</option>
                                                                                <option value="5">Mail</option>
                                                                                <option value="6">Security</option>
                                                                                <option value="19">Exchange</option>
                                                                                <option value="21">SMS</option>
                                                                                <option value="27">Public</option>
                                                                                <option value="15">Reccurent Payment
                                                                                </option>
                                                                            </select></td>
                                                                        <td width="30"></td>
                                                                        <td>
                                                                            <input type="submit" value="Поиск"
                                                                                   name="transaction-search-submit"
                                                                                   class="button" id="sbt">
                                                                            <input type="submit" value="Очистка фильтра"
                                                                                   name="transaction-search-clear"
                                                                                   class="button">
                                                                            <input type="button"
                                                                                   value="Скачать CSV историю"
                                                                                   name="button" class="button">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">До</td>
                                                                        <td>
                                                                            <input type="text" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}"
                                                                                   name="created2" default="12/17/2018"
                                                                                   size="10"
                                                                                   style="float: left; margin-right: 5px;">
                                                                            <a href="#"
                                                                               name="anchor2" id="anchor2">
                                                                                <img src="/public/cabinet/calendar.jpg"
                                                                                     alt="" width="16" height="15"
                                                                                     border="0">
                                                                            </a>
                                                                        </td>
                                                                        <td align="right">Сумма до</td>
                                                                        <td><input type="text" value="" name="amount2"
                                                                                   size="10"></td>
                                                                        <td align="right">Действие</td>
                                                                        <td><select name="action">
                                                                                <option></option>
                                                                                <option value="3">Change</option>
                                                                                <option value="4">Create</option>
                                                                                <option value="6">Make</option>
                                                                                <option value="8">Cancel</option>
                                                                                <option value="9">Send</option>
                                                                                <option value="12">Complete</option>
                                                                                <option value="13">Discard</option>
                                                                                <option value="15">Add</option>
                                                                                <option value="18">Receive</option>
                                                                                <option value="1">Login</option>
                                                                                <option value="2">Logout</option>
                                                                                <option value="20">Exchange</option>
                                                                                <option value="21">Transfer</option>
                                                                            </select></td>
                                                                        <td colspan="2"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </form>
                                                            <div id="dates"
                                                                 style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <br><br>
                                                <table border="0" width="98%" cellpadding="3" cellspacing="0">
                                                    <tbody>
                                                    <tr valign="top">
                                                        <td width="100" height="31"><b><a href="#">Создан_↓</a></b></td>
                                                        <td width="70" height="31"><b><a href="#">Объект</a></b></td>
                                                        <td width="70" height="31"><b><a href="#">Действие</a></b></td>
                                                        <td width="100" height="31"><b><a href="#">Сумма</a></b></td>
                                                        <td height="31"><b>Подробности</b></td>
                                                    </tr>
                                                    @foreach($logs as $key => $log)
                                                        @php
                                                            $x1 = $log->amount*100;
                                                            $ost100 = $x1%100;
                                                            $ost10 = $x1%10;

                                                            if ($ost100 > 0){
                                                                if($ost10>0){
                                                                    $dot = 2;
                                                                }else{
                                                                    $dot = 1;
                                                                }
                                                            }else{
                                                                $dot = 2;
                                                            }
                                                        @endphp
                                                        <tr>
                                                            <td colspan="7"><img src="/public/cabinet/red.gif"
                                                                                 width="100%"
                                                                                 height="1">
                                                            </td>
                                                        </tr>
                                                        {{--<tr class="even">--}}
                                                        <tr class="{{($key%2==0) ? 'odd' : 'even'}}">
                                                            <td style="line-height:200%;">{{\Carbon\Carbon::parse($log->sort_date)->format('m.d.y H:i')}}</td>
                                                            <td style="line-height:200%;">{{$log->type->title}}</td>
                                                            <td style="line-height:200%;">{{$log->action->title}}</td>
                                                            <td style="line-height:200%; {{($log->action_id==13) ? ' color: red' : 'color: green'}}">{{($log->action_id==13) ? '-' : ''}}{{($log->action_id==9) ? '+' : ''}}{{($log->action_id != 10 && $log->action_id != 11) ? number_format($log->amount, $dot, '.', '') : ''}}</td>
                                                            @if ($log->action_id == 9)
                                                                <td style="line-height:200%;"
                                                                    colspan="2">{{$log->action->title . ' Payment ' . number_format($log->amount, $dot, '.', '') . ' ' . $log->wallet_type->title . ' from account ' . $log->from_wallet . '  to account  ' . $log->to_wallet . '. Batch: ' . $log->batch . '. Memo: ' . $log->memo   }}</td>
                                                            @elseif($log->action_id == 13)
                                                                <td style="line-height:200%;"
                                                                    colspan="2">{{'Sent' . ' Payment: ' . number_format($log->amount, $dot, '.', '') . ' ' . $log->wallet_type->title . ' to account ' . $log->to_wallet . '  from  ' . $log->from_wallet . '. Batch: ' . $log->batch . '. Memo: ' . $log->memo   }}</td>
                                                            @elseif($log->action_id == 10)
                                                                <td style="line-height:200%;"
                                                                    colspan="2">{{$log->action->title . ' ' . ' IP : ' . $log->ip}}</td>
                                                            @elseif($log->action_id == 11)
                                                                <td style="line-height:200%;" colspan="2"></td>
                                                            @endif

                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <div style="position:relative">
                                                    <div style="position:absolute;top:5px;right:30px; float: right">
                                                        Результатов на
                                                        страницу:
                                                        <select name="perpage" onchange="my_redirect(this.value)">
                                                            <option {{$paginate==10 ? 'selected' : ''}} value="10">10</option>
                                                            <option {{$paginate==20 ? 'selected' : ''}} value="20">20</option>
                                                            <option {{$paginate==30 ? 'selected' : ''}} value="30">30</option>
                                                            <option {{$paginate==50 ? 'selected' : ''}} value="50">50</option>
                                                            <option {{$paginate==200 ? 'selected' : ''}} value="200">200</option>
                                                        </select></div>
                                                    <table border="0" cellspacing="5" cellpadding="0" id="pager"
                                                           align="center">
                                                        <tbody>
                                                        <tr>
                                                            <td class="first_end"
                                                                    @php
                                                                        if (empty($paginations['per_page']))
                                                                            echo "style='border: 0'";
                                                                    @endphp
                                                            >
                                                                @if (!empty($paginations['per_page']))
                                                                    <a style="margin: auto 2%"  href="{{$paginations['per_page']}}">←</a>
                                                                @endif
                                                            </td>
                                                            <td class="first_end"
                                                                    @php
                                                                        if (empty($paginations['next_page']))
                                                                            echo "style='border: 0'";
                                                                    @endphp
                                                            >
                                                                @if (!empty($paginations['next_page']))
                                                                    <a style="margin: auto 2%" href="{{$paginations['next_page']}}">→</a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <br><br>
                                                </div>
                                            </div>

                                        </div>
                                    </td>
                                    <td background="/public/cabinet/tright.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right"><img src="/public/cabinet/bottomleft.gif"></div>
                                    </td>
                                    <td background="/public/cabinet/bottom.gif" colspan="2">
                                        <div align="center"><img src="/public/cabinet/bottomleft.gif" width="1"
                                                                 height="1">
                                        </div>
                                    </td>
                                    <td><img src="/public/cabinet/rightbottom.gif" width="10" height="17"></td>
                                </tr>
                                </tbody>
                            </table>

                            <br>


                        </td>
                    </tr>

                    <tr valign="middle">
                        <td bgcolor="#FFFFFF" valign="middle" height="12"><img src="/public/cabinet/red.gif"
                                                                               width="100%"
                                                                               height="1"></td>
                    </tr>

                    </tbody>
                </table>
            </td>
            <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24" height="26"></td>
        </tr>
        </tbody>
    </table>

    <!--FOOTER-->

    @include('nav_menu.footer')

    <script>
        function my_redirect(val) {
            window.location = "{{$_SERVER['APP_URL']}}" + "/history.html/" + val;
        }

    </script>

@endsection