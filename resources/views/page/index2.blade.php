
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Perfect Money – новое поколение платежных систем Интернета. Платежная система для денежных переводов</title>
<META NAME="Keywords" CONTENT="купить,продать,поменять,онлайн,евалюта,электронная валюта,платежная система, пеймент процессор, платежный шлюз,api мерчант,merchant, оплата,средство,онлайн банкинг,денежный перевод,финансовый сервис,платежный сервис,швейцарская,безопасность, деньги">
<META name="description" content="Платежная система Perfect Money открывает самый простой и безопасный финансовый сервис для совершения денежных переводов по всему миру.Принимайте электронную валюту, банковские переводы и смс платежи на Вашем вебсайте.Покупайте золото, отправляйте или принимайте деньги с наиболее безопасной платежной системой в Интернете">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">

body { max-width:1650px}
.top {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8pt}
.req {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9pt; color: #FF0000}
td {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: #333333}
.ag {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #333333}
h2 {  font-family: Arial, Helvetica, sans-serif; font-size: 15pt; color: #333333}
#TJK_ToggleON,#TJK_ToggleOFF {display:none}
.menu {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt}
.txt {  text-align: justify}
a {  color: #990000}
</style>
<link rel="StyleSheet" href="/public/home/style.css" type="text/css">
<link rel="StyleSheet" href="/public/home/colorbox_publics.css" type="text/css">
<script type="text/javascript" src="/public/home/jquery.comp.js"></script>
<script type="text/javascript">
$j = jQuery.noConflict();	
jQuery(document).ready(function(){  
	$j("#memo").addClass("input");
	$j("input").addClass("input");
	$j(":submit").addClass("submit");
});
</script>
<script type="text/javascript" src="/public/home/jquery.1.9.min.js"></script>
<script type="text/javascript" src="/public/home/jquery.colorbox-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){  
	$('a[href="https://perfectmoney.com/tour.html"]').colorbox({href:"/tour.html",scrolling:false,initialWidth:1000,initialHeight:590,innerWidth:977,width:1000,height:590,fixed:true,top:60,left:350});
});
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="general">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td height="43"><br><img alt="Payment System Gold USD EUR" src="/public/home/blank.gif" width="950" height="5">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="35%"><img alt="E-Currency Payment System" src="/public/home/blank.gif" width="14" height="26"><a href="{{route('home')}}"><img alt="Perfect Money Payment System" src="/public/home/logo3.png" border=0></a></td>
          <td valign="bottom" width="65%">           
			<div align="right">
				<form method="post" name="f" action="#">
					<table cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td valign="middle"><img
										src="/public/home/RU.GIF">&nbsp;<br><br>
							</td>
							<td><select name="lang"
										onchange="if (this.value != &#39;&#39;) document.f.submit()">&nbsp;&nbsp;<option
											value=""> --- ---
									</option>
									<option value="en_US">English</option>
									<option value="de_DE">Deutsch</option>
									<option value="el_GR">Ελληνικά</option>
									<option value="zh_CN">中文</option>
									<option value="ja_JP">日本語</option>
									<option value="ko_KR">한국어</option>
									<option value="es_ES">Español</option>
									<option value="fr_FR">Français</option>
									<option value="ru_RU" selected="">Русский</option>
									<option value="uk_UA">Українська</option>
									<option value="it_IT">Italiano</option>
									<option value="pt_PT">Português</option>
									<option value="ar_AE">العربية</option>
									<option value="fa_IR">فارسی</option>
									<option value="th_TH">ไทย</option>
									<option value="id_ID">Indonesia</option>
									<option value="ms_MY">Malaysian</option>
									<option value="tr_TR">Türkçe</option>
									<option value="pl_PL">Polski</option>
									<option value="ro_RO">Român</option>
									<option value="hi_IN">Hindi</option>
									<option value="ur_IN">Urdu</option>
									<option value="vi_VN">Vietnam</option>&nbsp;&nbsp;</select><img
										alt="Switzerland E-Currency"
										src="/public/home/blank.gif"
										width="42" height="5"><br>
								<img alt="Switzerland E-Currency"
									 src="/public/home/blank.gif"
									 width="1" height="15"></td>
						</tr>
						</tbody>
					</table>
				</form>

				<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
					@if (Route::has('login'))

							@auth
								<a href="{{route('cabinet')}}"><font color="#004e97">Мой
										аккаунт</font></a>
							@else
								@if (Route::has('register'))
									{{--<a href="{{ route('register') }}"><font--}}
									<a href="#"><font
												color="#000000">Регистрация</font></a>&nbsp;&nbsp;&nbsp;
								@endif
								<font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
										href="{{ route('login') }}"><font
											color="#000000">Вход</font></a>
								&nbsp;&nbsp;&nbsp;
								<font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
										href="#"><font
											color="#000000">Обменные
										пункты</font></a>
							@endauth

					@endif
					&nbsp;&nbsp;&nbsp;<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
						<font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
								href="#" class="cboxElement"><font
									color="#000000">Тур</font></a>&nbsp;&nbsp;&nbsp;
						<font color="#999999"></font></font><font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
							href="#"><font color="#000000">Помощь</font></a>&nbsp;&nbsp;&nbsp;<font
							color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
							href="#"><font color="#000000">Центр
							безопасности</font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>

				</font>
			</div>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
</table>
<!--
<div id="season" style="position:absolute; width:1px; height:1px; z-index:1; left: 285px; top: 17px"><img src="/public/home/season1_1.png"></div>
-->
<div id="season" style="position:absolute; width:1px; height:1px; z-index:-1; left: 356px; top: 5px"><img src="/public/home/summer4.gif"></div>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td>
     <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="0B0A0C">
<tr>
	<td height="8" width="2%"><img alt="Buy Gold Metal, Buy USD EURO currency online, Payment System" src="/public/home/top2-70.png"></td>
   	<td height="8" bgcolor="#0A0A0A">
	<div align="center">
    <table width="216" border="0" cellspacing="0" cellpadding="0">
    <tr>
    	<td><img alt="E-Currency Payment System" src="/public/home/mid3-70.png"></td>
	</tr>
    <tr>
    	<td>&nbsp; </td>
	</tr>
	</table>
	</div>
	</td>
	<td height="8" width="1%">
		<div align="right"><img alt="fast,easy,comfortable - way to develop your money" src="/public/home/right2-70.png" width="18" height="167"></div>
	</td>
</tr>
</table>

    </td>
  </tr>
  <tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tbody>
	<tr>
		<td width="36"><img style="display: block"
					src="/public/home/left-70.gif"
					width="36" height="26"></td>
		<td colspan="2" bgcolor="B01111">
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
				<tr>
					@guest
						<td width="100">&nbsp;<a class="no" href="{{route('login')}}"><font
										face="Verdana, Arial, Helvetica, sans-serif" size="2"
										color="#FFFFFF">Вход</font></a>
						</td>
					@endguest
					<td width="150"><img
								src="/public/home/blank.gif"
								width="150" height="10"></td>
					<td nowrap="">
						<div id="menuOver">
							<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
								<div class="menu" style="min-width:780px"><a
											href="{{route('home')}}"><span>Главная</span></a>
									<a href="#" class="selectedd"><span>О Нас</span></a>
									<a href="#"><span>Возможности</span></a>
									<a href="#"><span>Комиссии</span></a>
									<a href="#"><span>E-Ваучеры</span></a>
									<a href="#"><span>Гарантии</span></a>
									<a href="#"><span>F.A.Q.</span></a>
									<a href="#"><span>Обратная Связь</span></a>
								</div>
							</font>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
		<td width="4%" bgcolor="B01111" style="border-bottom-right-radius: 7px" valign="middle">

		</td>
	</tr>
	</tbody>
</table>

</td>
</tr>                                                                               
<tr>
    <td><img src="/public/home/blank.gif" width="820" height="1"></td>
  </tr>
</table>
<meta name="apple-itunes-app" content="app-id=id653398845">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="23"><img src="/public/home/blank.gif" width="23" height="26"></td>
          <td> 
            
      <table border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="310" valign="top" background="/public/home/left33.gif"><font face="Arial, Helvetica, sans-serif" size="3"><br>
            <font face="Arial, Helvetica, sans-serif" size="3"><b>Обменные курсы в <font color='#F01010'>PM</font></b></font><br>
<table width="270" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td height="2">
		<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#999999">USD, EUR:</font>
		<font face="Verdana, Arial, Helvetica, sans-serif" size="1"><br><br></font>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="8"><img src="/public/home/left_c.gif" width="8" height="36"></td>
			<td bgcolor="E8E8E8" width="50%">
			<div align="right">
				<font face="Arial, Helvetica, sans-serif" size="1">
				<font face="Verdana, Arial, Helvetica, sans-serif"><b>USD</b></font>
				<font face="Verdana, Arial, Helvetica, sans-serif"> / <b>EUR</b>&nbsp;&nbsp;0.866&nbsp;&nbsp;
				<font color="#CC0000">|</font></font></font></div>
			</td>
			<td bgcolor="E8E8E8" width="50%">
			<div align="left">
				<font face="Arial, Helvetica, sans-serif" size="1">
					<font face="Verdana, Arial, Helvetica, sans-serif" color="#CC0000">|</font>
					<font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;<b>EUR</b>
						<font face="Verdana, Arial, Helvetica, sans-serif"> / </font>
					</font>
					<font face="Verdana, Arial, Helvetica, sans-serif"><b>USD</b>&nbsp;&nbsp;1.108</font>
				</font>
			</div>
			</td>
			<td width="8" valign="middle">
				<div align="right"><img src="/public/home/right_c.gif" width="8" height="36"></div>
			</td>
		</tr>
		</table>
		<br><img src="/public/home/21.graph" width="265" height="130"><br>
		<font face="Verdana, Arial, Helvetica, sans-serif" size="1"><br>
			<font color="#999999">BTC:</font><br><br>
		</font>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="8"><img src="/public/home/left_c.gif" width="8" height="36"></td>
			<td bgcolor="E8E8E8" width="50%">
			<div align="right">
				<font face="Arial, Helvetica, sans-serif" size="1">
					<font face="Arial, Helvetica, sans-serif" size="1">
						<font face="Verdana, Arial, Helvetica, sans-serif"><b>USD</b></font>
						<font face="Verdana, Arial, Helvetica, sans-serif"> &gt;&gt; </font>
					</font>
					<font face="Verdana, Arial, Helvetica, sans-serif" size="1">3305.55&nbsp;&nbsp;
						<font color="#CC0000">|</font>
					</font>
				</font>
			</div>
			</td>
			<td bgcolor="E8E8E8" width="50%">
				<font face="Arial, Helvetica, sans-serif" size="1">
					<font face="Verdana, Arial, Helvetica, sans-serif" color="#CC0000">|</font>
					<font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;<b>EUR</b></font>
					<font face="Arial, Helvetica, sans-serif" size="1">
						<font face="Verdana, Arial, Helvetica, sans-serif"> &gt;&gt; </font>
					</font>
					<font face="Verdana, Arial, Helvetica, sans-serif" size="1">2902.32</font>
				</font>
			</td>
			<td width="8" valign="middle">
			<div align="right">
				<img src="/public/home/right_c.gif" width="8" height="36">
			</div>
			</td>
		</tr>
		</table>
		<br><img src="/public/home/71.graph" width="265" height="130"><br>
		<font face="Verdana, Arial, Helvetica, sans-serif" size="1"><br>
			<font color="#999999">GOLD Bid Price /oz:</font><br><br>
		</font>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="8"><img src="/public/home/left_c.gif" width="8" height="36"></td>
			<td bgcolor="E8E8E8" width="50%">
			<div align="right">
				<font face="Arial, Helvetica, sans-serif" size="1">
					<font face="Arial, Helvetica, sans-serif" size="1">
						<font face="Verdana, Arial, Helvetica, sans-serif"><b>USD</b></font>
						<font face="Verdana, Arial, Helvetica, sans-serif"> &gt;&gt; </font>
					</font>
					<font face="Verdana, Arial, Helvetica, sans-serif" size="1">1213.798&nbsp;&nbsp;
						<font color="#CC0000">|</font>
					</font>
				</font>
			</div>
			</td>
			<td bgcolor="E8E8E8" width="50%">
				<font face="Arial, Helvetica, sans-serif" size="1">
					<font face="Verdana, Arial, Helvetica, sans-serif" color="#CC0000">|</font>
					<font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;<b>EUR</b></font>
					<font face="Arial, Helvetica, sans-serif" size="1">
						<font face="Verdana, Arial, Helvetica, sans-serif"> &gt;&gt; </font>
					</font>
					<font face="Verdana, Arial, Helvetica, sans-serif" size="1">1073.24</font>
				</font>
			</td>
			<td width="8" valign="middle">
			<div align="right">
				<img src="/public/home/right_c.gif" width="8" height="36">
			</div>
			</td>
		</tr>
		</table>
		<br><img src="/public/home/31.graph" width="265" height="130">
	</td>
</tr>
</table>
<br>
<div style="width:270px">
<font face="Arial, Helvetica, sans-serif" size="3"><b>Опрос</b></font>
<font face="Arial, Helvetica, sans-serif" size="2"><br><br>
Perfect Money: Качество обслуживания<br><br>
<a href="/statistics.html">Просмотр результатов в режиме реального времени</a> &raquo;</font>
</div>
</font> 
            <div align="left"><br>
              <div class="arabic">


			  
<div class="a1">
<br>
<font face="Arial, Helvetica, sans-serif" size="3"><b>Часто задаваемые вопросы</b></font> <br>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF0000"><b>
<br>
</b></font>
<table width="265" border="0" cellspacing="0" cellpadding="0" align="left">
<tr>
	<td>
    <p class="txt"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF0000"><font face="Arial, Helvetica, sans-serif" size="2" color="B01111">Есть ли комиссия за вывод средств на Bitcoin?</font><font face="Arial, Helvetica, sans-serif" size="2"><br>
    <br>
    </font>
	<font color="#000000" face="Arial, Helvetica, sans-serif" size="2">Да, система взимает 0,5% от суммы платежа за каждую операцию по выводу средств в Bitcoin.</font><br><br>
    </td>
</tr>
<tr>
	<td>
    <p class="txt"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF0000"><font face="Arial, Helvetica, sans-serif" size="2" color="B01111">Какие варианты приема платежей будут у моих покупателей?</font><font face="Arial, Helvetica, sans-serif" size="2"><br>
    <br>
    </font>
	<font color="#000000" face="Arial, Helvetica, sans-serif" size="2">Подключив один из модулей API Merchant к вашему магазину/сайту, вы сразу же обеспечите своих клиентов еще одним платежным инструментом, включающим оплату с помощью валюты Perfect Money, e-Voucher-ов, СМС платежей и банковских переводов.</font><br><br>
    </td>
</tr>
<tr>
  <td>
  <p><font face="Arial, Helvetica, sans-serif">
<font size="2"><a href="#">Больше ответов и вопросов</a> &raquo;</font></font></p>
<br><br>
  </td>
</tr>
</table>
</div>



</div>
<br>
<br>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF0000"><b> 
              </b><img src="/public/home/blank.gif" width="307" height="26"></font></div>
          </td>
          <td valign="top" align="left"> 
			<p><font face="Arial, Helvetica, sans-serif" size="3"><b><br>
<font size="4">Добро пожаловать в Perfect Money!</font></b></font></p>
<p class="txt"><font face="Arial, Helvetica, sans-serif" size="2">Perfect Money - это сервис, позволяющий пользователям производить моментальные платежи и финансовые операции в Интернете, и открывающий уникальные возможности для Интернет пользователей и владельцев интернет бизнеса.<br>
Цель Perfect Money вывести финансовые операции в Интернете на идеальный уровень!</font></p>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table border="0" cellspacing="0" cellpadding="0" id="promo_nav1" class="nav">
        <tr>
          <td class="ina_left" width="14" height="30"><img src="/public/home/spacer.gif"></td>
          <td onClick="showPromo('#promo1','#promo_nav1')" class="ina_center"><a href="javascript:;">О системе</a></td>
          <td class="ina_right" width="12" height="30"><img src="/public/home/spacer.gif"></td>
        </tr>
      </table>
    </td>
    <td>
      <table border="0" cellspacing="0" cellpadding="0" id="promo_nav2" class="nav">
        <tr>
          <td class="ina_left" width="14" height="30"><img src="/public/home/spacer.gif"></td>
          <td class="ina_center" onClick="showPromo('#promo2','#promo_nav2')"><a href="javascript:;">Как ввести средства</a></td>
          <td class="ina_right" width="12" height="30"><img src="/public/home/spacer.gif"></td>
        </tr>
      </table>
    </td>
    <td>
      <table border="0" cellspacing="0" cellpadding="0" id="promo_nav3" class="nav">
        <tr>
          <td class="ina_left" width="14" height="30"><img src="/public/home/spacer.gif"></td>
          <td class="ina_center" onClick="showPromo('#promo3','#promo_nav3')"><a href="javascript:;">Как вывести средства </a></td>
          <td class="ina_right" width="12" height="30"><img src="/public/home/spacer.gif"></td>
        </tr>
      </table>
    </td>
    <td>
      <table border="0" cellspacing="0" cellpadding="0" id="promo_nav4" class="nav">
        <tr>
          <td class="ina_left" width="14" height="30"><img src="/public/home/spacer.gif"></td>
          <td class="ina_center" onClick="showPromo('#promo4','#promo_nav4')"><a href="javascript:;">e-Voucher</a></td>
          <td class="ina_right" width="12" height="30"><img src="/public/home/spacer.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="700" bgcolor="#D6D6D6">
  <tr>
    <td width="10"><img src="/public/home/blank.gif" width="1" height="1"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="15" width="700" bgcolor="#F7F7F7" id="promo1" class="promo">
  <tr>
    <td> <font color="#990000"><b><font size="3">Perfect Money предоставляет уникальные возможности как для бизнес, так и для личных счетов.  </font></b></font><br>
      <br>
      <h1 class="home">Используя платежную систему Perfect Money Вы можете:</h1>
      <br>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="10">&nbsp;</td>
          <td>
            <table width="600" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><img src="/public/home/arrow.gif">
                  Производить взаиморасчеты между пользователями  </font></td>
              </tr>
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><img src="/public/home/arrow.gif">
                  Получать платежи в различных бизнес проектах в Интернете  </font></td>
              </tr>
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><img src="/public/home/arrow.gif">
                  Производить регулярные платежи в Интернете</font></td>
              </tr>
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><img src="/public/home/arrow.gif">
                  Бережно хранить денежные средства на электронном счету и получать ежемесячные процентные выплаты с месячного остатка на счету </font></td>
              </tr>
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><img src="/public/home/arrow.gif">
                  Производить оплату за товары и услуги в Интернет магазинах</font></td>
              </tr>
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><img src="/public/home/arrow.gif">
                  Приобрести Bitcoin, Золото, USD и EUR валюты онлайн  </font></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="15" width="700" bgcolor="#F7F7F7" id="promo2" class="promo">
  <tr>
    <td>Для того, чтобы производить оплату через Интернет с помощью валюты Perfect Money Вам сначала нужно полнить свой счет. Вы можете это сделать несколькими способами, которые позволят Вам превратить ваш реальный кошелек в виртуальный. <br>
      <br>
      <b><font color="#990000" size="2"></font></b><b><font color="#990000" size="2"></font></b><b><font color="#990000" size="2">Банковский перевод</font></b><br>
      <br>
      Для пополнения счета Вам необходимо отправить банковский перевод (SWIFT / IBAN) с Вашего счета в банке или принять платеж от 3 стороны, например, от Вашего бизнес-партнера. Предоставляя своим партнерам данные Вашего счета в Perfect Money, Вы получаете возможность принимать банковские переводы, даже если у Вас нет счета в банке.  <br>
      <br>
      <a href="/easy_way_deposit.html">Узнайте, насколько просто пополнить счет в Perfect Money банковским переводом и пользоваться широким спектром услуг платежной системы Perfect Money.</a><br>
      <br>
      Наши тарифы действительно очень низки. <br>
      <br>
      <b><font size="2" color="#990000">e-Voucher</font></b><br>
      <br>
      Выбирая данную опцию Вы покупаете e-Voucher с кодом активации в Интернете, и пополняете свой счет вводя этот код в специальную форму.  Так, например, Вы можете купить e- Voucher за валюту Western Union у одного из многочисленных обменных пунктов или на Интернет-аукционе. <br>
      <br>
      <b><font size="2" color="#990000">E-currency</font></b><br>
      <br>
     Вводите средства напрямую из других платежных систем.  Все платежи с помощью электронных валют зачисляются на счет моментально. <br>
      <br>
      <b><font color="#990000" size="2">Сертифицированные Обменные Сервисы - Партнеры</font><br>
      <br>
      </b> Электронную валюту Perfect Money можно приобрести за другую е-валюту, системы Western Union, Money Gram или за наличные. Ввести средства с помощью Обменных Сервисов – Партнеров.<br>
      <br>
      <b><font color="#990000" size="2">Bitcoin</font><br>
      <br>
      </b>Сделать депозит используя криптовалюту Bitcoin. Мгновенно пополнить ваш счет Perfect Money B, после трех подтверждений.
</td>
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="15" width="700" bgcolor="#F7F7F7" id="promo3" class="promo">
  <tr>
    <td>Конвертировать электронную валюту Perfect Money в реальные деньги или другие средства оплаты очень легко. Вывести средства можно следующими способами: <br>
      <br>
      <b><font color="#990000" size="2">Банковский Перевод </font></b><br>
      <br>
   Вы можете выводить валюту Perfect Money посредством банковского перевода на Ваш онлайн счет в банке, или на счет 3 стороны.  Если Ваши бизнес партнеры или  сервис принимает только банковские переводы, Вы также можете вывести Perfect Money с помощью банковского платежа в качестве оплаты за товар или услугу. <br>
      <br>
      Наши тарифы действительно очень низки. <br>
      <br>
      <b><font color="#990000" size="2">e-Voucher</font></b><br>
      <br>
     Создайте свой личный сертификат e-Voucher  Perfect Money.  Это великолепная идея давать коды e-voucher-ов своим членам семьи или использовать их как бонусы. Также Вы можете продать свой e-Voucher за наличные.
<br>
      <br>
      <b><font color="#990000" size="2">Электронная валюта</font></b><br>
      <br>
      У Вас возможность выводить свои средства с аканта в Perfect Money на счет в другой электронной системе. <br>
      <br>
      <b><font color="#990000" size="2">Препейд карты</font></b><br>
      <br>
      Мы предоставляем клиентам возможность покупать препейд карты в Интернете не выходя из своего аккаунта в Perfect Money account. Например, Вы можете купить "Предоплаченную карту Visa" для безопасного и надежного шоппинга в Интернете.
      <p><b><font color="#990000" size="2">Сертифицированные Обменные Сервисы - Партнеры</font><br>
        </b><br>
        Валюту Perfect Money можно приобрести с помощью систем  Western Union, Money Gram. Также Вы можете обменять Perfect Money на любую другую электронную валюту или обналичить их в своем городе. <br>
        <br>
        Вывести средства с помощью Сертифицированных Обменных Сервисов – Партнеров. </p>
				<p><b><font color="#990000" size="2">Bitcoin</font><br>
        </b><br>
        Вывести средства на ваш кошелек Bitcoin. Perfect Money позволяет клиентам выводить средства со счетов Perfect Money B на любой кошелек Bitcoin. Все переводы обрабатываются в автоматическом режиме.<br>
        </p>
      </td>
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="15" width="700" bgcolor="#F7F7F7" id="promo4" class="promo">
  <tr>
    <td> <font color="#990000" size="3"><b>Задумывались ли Вы когда-нибудь о создании Вашей личной препейд-карты? </b></font><br>
      <br>
      Задумывались ли Вы о том, что Вы можете покупать виртуальные карты Perfect Money за вашу электронную валюту в Интернете, а потом конвертировать ее в валюту Perfect Money? Платежная система Perfect Money предоставляет Вам такую возможность и делает это реальностью!
      <p>Виртуальная препейд карта Perfect Money с кодом активации, называемая e-Voucher, всегда может быть создана любым пользователем  Perfect Money.</p>
      <p>e-Voucher – это специальный код, который Вы можете использовать для пополнения любого счета в системе Perfect Money. Также, Вы можете передать этот код другим пользователям, которые также могут использовать его для пополнения своего счета  в платежной системе Perfect Money в любое удобное для них время. </p>
      <p>Вы можете отправить код  e-Voucher на любой электронный адрес или номер мобильного телефона.</p>
      <p>Для создания e-Voucher Вам необходимо войти в Ваш аккаунт в Perfect Money, нажать на  "Вывод средств" и там кликнуть на "e-Voucher". Затем, Вам будет необходимо ввести стоимость e-Voucher- а и создать его. Созданные e-Voucher-ы всегда доступны в Депозитарии, котрый можно найти в разделе “Статистика.”  </p>
      <p>Если Вам необходимо пополнить Ваш счет в Perfect Money Вы можете приобрести e-Voucher в Интернете. Затем Вам необходимо войти в Ваш аккаунт в Perfect Money, нажать там на "Ввод средств" и выбрать "e- Voucher" в качестве инструмента пополнения. </p>
      <p>e-Voucher – это удобный способ оплачивать товары и услуги в Интернете, даже если Ваш клиент не имеет аккаунта в системе Perfect Money.</p>
    </td>
  </tr>
</table>

<p class="txt"><font face="Arial, Helvetica, sans-serif" size="2">В каждой операции, которая проводится системе Perfect Money,
мы стремимся предложить нашим клиентам совершенный сервис.
Мы знаем, у будущего есть Perfect Money, а у Вас - возможность быть нашим клиентом! </font></p>
<p class="txt"><font face="Arial, Helvetica, sans-serif" size="2">Вы новичок в Perfect Money? </font>
<font face="Arial, Helvetica, sans-serif">
<font size="2"><a href="#">Оправляйтесь в Тур по системе &raquo;</a></font></font> </p>
<div align="right">
      <p align="left"><img alt="No fees payment system" src="/public/home/nofees.gif"></p>
</div>
<script type="text/javascript">
function showPromo(id, elem)
{
  $('table.promo').hide();
  $(id).show();

  if (elem)
  {
    $("table.nav td").each(
      function()
      {
        switch (this.className) {
          case 'act_left' : case 'ina_left': classname = 'ina_left'; break;
          case 'act_center' : case 'ina_center': classname = 'ina_center'; break;
          case 'act_right' : case 'ina_right' : classname = 'ina_right'; break;
        }

        this.className = classname;
      }
    );

    var current = $(elem+' td');

    current.get(0).className = 'act_left';
    current.get(1).className = 'act_center';
    current.get(2).className = 'act_right';
  }
}

showPromo('#promo1','#promo_nav1');
</script>
 
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class=txt><div class="arabic">
<p class="faq">
<font face="Arial, Helvetica, sans-serif" size="3"><b>Команда платежной системы Perfect Money поздравляет Вас с Рождеством и Новым 2020 годом!</b></font>
<br>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#666666">27.12.19</font>
<BR><BR>
<font face="Arial, Helvetica, sans-serif" size="2">Дорогие пользователи платежной системы Perfect Money,<br />
<br />
Вот уже более 12 лет команда компании Perfect Money упорно работает над тем чтобы каждый из вас мог удобно производить финансовые транзакции в любое время суток. Оглядываясь назад, приходит понимание о том насколько большой Путь мы проделали вместе с Вами. На этой дороге чаще всего нам сопутствовал успех но не обходилось и без проблем которые мы уверено решали всей командой и тем самым становились опытнее и сильнее. </font><p><a href="newsview.html?id=367">Читать далее</a> &raquo;</p>
</font>
<p><br><font face="Arial, Helvetica, sans-serif"><b><font size="2"> Больше новостей:<br></font></b><br>
<div class="newstitle"><a href="#">Мы добавили возможность пополнения/снятия средств через пункты приема наличных денег</a> &raquo; <span class="date">18.04.19</span></div><img src="/public/home/blank.gif" width="10" height="5"><br>
<div class="newstitle"><a href="#">Компания Perfect Money поздравляет Всех с наступающим Рождеством и Новым 2019 годом!</a> &raquo; <span class="date">25.12.18</span></div><img src="/public/home/blank.gif" width="10" height="5"><br>
<div class="newstitle"><a href="#">Команда платежной системы Perfect Money поздравляет своих пользователей с Рождеством и Новым 2018 Годом!</a> &raquo; <span class="date">28.12.17</span></div><img src="/public/home/blank.gif" width="10" height="5"><br>
<div class="newstitle"><a href="#">Теперь наши приложения для iOS/Android стали мультиязычными</a> &raquo; <span class="date">20.08.17</span></div><img src="/public/home/blank.gif" width="10" height="5"><br>
<br><a href="#"><font size="2">Архив новостей</font></a> &raquo;</font></p><br>
</div>
</td>
              </tr>
            </table>
            <br>
            <br>
          </td>
          <td valign="top" background="/public/home/left36.gif" id="promo" width="200"> 
          <div style="padding-left: 40px;padding-top:18px;" class="txt arabic">
<font face="Arial, Helvetica, sans-serif" size="2">
<font face="Arial, Helvetica, sans-serif" size="3"><b><font color='#F01010'>PM</font> E-Vouchers</b></font> <br><br>
<a href="/evoucher-info.html"><img src="/public/home/e-voucher.png"></a><br><br>
<font face="Arial, Helvetica, sans-serif" size="3"><font face="Arial, Helvetica, sans-serif" size="2" class=""><b>Кажется виртуальным,<br>но работает как реальные деньги</b></font></font>
<ul style="padding:0px;padding-left:10px;">
	<li>Создайте e-Voucher на любую сумму.</li><br>
	<li>Переводить деньги любому получателю по всему миру (адресат получит номер e-Voucher и код активации по SMS, даже если у него/нее нет счета PM).</li><br>
	<li>Платить онлайн за товары и услуги безопасно и конфиденциально.</li><br>
	<li>Легко обменивать e-Voucher на любую другую электронную валюту или наличные через обменные сервисы.</li>
</ul>
<a href="#">Читать далее &raquo;</a><br><br><br>
<font face="Arial, Helvetica, sans-serif" size="3"> <font face="Arial, Helvetica, sans-serif" size="3"><b>Прием платежей Perfect Money - гарантия роста дохода!</b></font></font><br>
<br>
<a href="#"><img src="/public/home/pm_accepted_index.jpg" width="248" height="153"></a><br><br>
Добавляя Perfect Money в качестве метода оплаты на вашем сайте, вы значительно повышаете свои продажи.<br><br>
Простая настройка SCI, интеграция с десятками CMS, более 10,000,000 держателей аккаунтов Perfect Money.<br><br>
<a href="#">Добавьте корзину Perfect Money на ваш сайт и откройте ваши услуги для миллионов активных пользователй Perfect Money. &raquo;</a> <br><br><br>
<font face="Arial, Helvetica, sans-serif" size="3"> <font face="Arial, Helvetica, sans-serif" size="3"><b>Легкая интеграция в программу Shopping Cart </b></font></font><br><br>

<img src="/public/home/total4_mini.gif" width="320" height="57"><br><br>
Наш технический отдел разработал плагины для наиболее популярных скриптов “электронных тележек.” Теперь вы можете добавить Perfect Money в список способов оплаты на Вашем сайте, загрузив необходимый плагин в уже существующую программу shopping cart. Оплата с помощью СМС, банковских переводов, e-Voucher-ов Perfect Money, карт предоплаты PM Prepaid Cards может стать возможной в Вашем уже существующей программе электронной торговли!<br><br>
<a href="#">Подробная информация об API и Плагинах Perfect Money &raquo;</a><br><br><br><br>
</font>
</div>
            
</td>
        </tr>
      </table>
          </td>
          <td width="44" valign="middle"><img src="/public/home/blank.gif" width="44" height="26"></td>
        </tr>
      </table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td height="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="1%"><img src="/public/home/left2.gif" width="19" height="6"></td>
          <td width="98%" bgcolor="C61313"><img src="/public/home/blank.gif" width="1" height="1"></td>
          <td width="1%" bgcolor="B01111" valign="middle"><img src="/public/home/right2.gif" width="19" height="6"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="341" bgcolor="#ffffff" valign="middle" height="56"> 
      <div align="left">&nbsp;&nbsp;
<!--
<a href="https://itunes.apple.com/gb/app/perfect-money/id653398845?mt=8" target="_blank"><img src="/img/mobile/ios_app_store.png" width="155" height="54"></a>
<a href="https://play.google.com/store/apps/details?id=com.touchin.perfectmoney&hl=en" target="_blank"><img src="/img/mobile/googleplayicon.png" width="155" height="54"></a>
-->

</div>
    </td>
    <td bgcolor="#ffffff" valign="top" height="56">
      <table width="100%" border="0" cellspacing="5" cellpadding="5" align="right">
        <tr>
          <td> 
            <div align="right"><small><font face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
              Perfect Money – новое поколение платежных систем Интернета. Платежная система для денежных переводов&nbsp;<br>&copy;
              2007-2021 SR & I. All rights reserved.&nbsp;<br>
              <a href="/promotion_materials.html"><font color="#b50b0b">Партнерская программа</font></a>
| <a href="/sample-api.html"><font color="#b50b0b">Perfect
Money API</font></a> | <a href="/legal.html"><font color="#b50b0b">Юридический аспект</font></a>
| <a href="/privacy.html"><font color="#b50b0b">Правовое положение</font></a><small><font face="Verdana, Arial, Helvetica, sans-serif" size="1">
 | <a href="/tos.html"><font color="#b50b0b">Условия использования</font></a></font></small><font face="Verdana, Arial, Helvetica, sans-serif" size="1">
| <a href="/aml.html"><font color="#b50b0b">AML</font></a></font></small><br><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/sitemap.html"><font color="#b50b0b">Карта сайта</font></a></font></font></small>

					</div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>