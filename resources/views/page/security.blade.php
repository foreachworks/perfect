@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

    <table width="100%" class="qwer" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26"></td>
            <td width="94%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr background="/public/cabinet/bgg.gif">
                        <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">
                                        <span id="actions"></span>
                                        <script type="text/javascript">
                                            function getUserEvents() {
                                                $j.get('/user/events.asp?section=history_main', null, function (data) {
                                                    if (data && data.length > 1) {
                                                        var params = JSON.parse(data);
                                                        $j('#actions').html(params.actions);
                                                        $j('#messages').html(params.news);
                                                    }
                                                });
                                                setTimeout('getUserEvents()', 100000);
                                            }

                                            setTimeout('getUserEvents()', 1500);
                                        </script>
                                        <noscript>
                                            <img src="/img/attention.gif" height="16" widht="16"> <font color="red"><b>Please,
                                                    enable Javascript for this site</b></font>
                                        </noscript>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        @include('nav_menu.to_date')
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img src="/public/cabinet/red.gif" width="100%"
                                                                    height="1">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            @include('nav_menu.payment')
                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', 'd8384dc6fd5d9010c43f88ce04bff565', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>

                            <br>

								<table width=700 border="0" cellspacing="0" cellpadding="0">
								  <tr> 
									<td width="23" valign="top"><img src="/public/cabinet/tlefttop.gif" width="23" height="42"></td>
									<td background="/public/cabinet/tbg.gif" valign="bottom" nowrap>
								<font color="004E97" face="Arial, Helvetica, sans-serif" size="5"><img src="/public/cabinet/blank.gif" width="15" height="10">
								Безопасность аккаунта
								</font>
								</td>
									<td background="/public/cabinet/trighttop.gif" width="999"><img src="/public/cabinet/tcenter.gif" width="60" height="42"></td>
									<td valign="bottom" width="10"><img src="/public/cabinet/trr.gif" width="10" height="42"></td>
								  </tr>
								  <tr> 
									<td background="/public/cabinet/tleft.gif">&nbsp;</td>
									<td colspan="2" bgcolor="#FFFFFF"> 
								<p>&nbsp;</p>      

								<div style="padding: 0px 40px 0px 20px">
								<div class="arabic">
								<p>Постоянно следить за безопасностью аккаунта – лучший способ оставаться защищенным.</p>
								<p>Настройте индивидуально уровень безопасности, отвечающий Вашим потребностям в защите своего аккаунта.</p>

								  <table width="98%" border="0" cellspacing="0" cellpadding="0">
									<tr>
									  <td><div class=box3 style="background-color:#fff;background:none">
										<form method="post">
										  <table width="600" border="0" cellspacing="0" cellpadding="10">
											<tr>
											  <td width="71" valign="top"><img src="/public/cabinet/ip.gif" /></td>
											  <? ($security->ip == 0) ? $ip = '/public/cabinet/off.gif' : $ip = '/public/cabinet/on.gif' ?>
											  <? ($security->ip == 0) ? $ip_bitton = ' Включить ' : $ip_bitton = ' Отключить ' ?>
											  <td width="529"> <b><img src="{{ $ip }}"> Проверка подлинности пользователя <br><br><input type='button' style="background-image: url(/img/button_bg.gif)" onclick="ChangeSecurity(1);" value='{{ $ip_bitton }}' class='button'></b><br>
												<br>
												При входе в систему с неизвестного IP-адреса Perfect Money отправляет Вам сообщение на e-mail с кодом подтверждения.</td>
											</tr>
											<tr>
											  <td width="71" valign="top">&nbsp;</td>
											  <td width="529">&nbsp;</td>
											</tr>
										  </table>
										</form>
										<br>
										<img src="/public/cabinet/e8.gif" width="100%" height="1"> <br>
										<form action="/user/savesms.asp" method="post">
										  <table width="600" border="0" cellspacing="0" cellpadding="10">
											<tr>
											  <td width="71" valign="top"><img src="/public/cabinet/sms1.gif" /></td>
											  <? ($security->sms == 0) ? $sms = '/public/cabinet/off.gif' : $sms = '/public/cabinet/on.gif' ?>
											  <? ($security->sms == 0) ? $sms_bitton = ' Включить ' : $sms_bitton = ' Отключить ' ?>
											  <td width="529"> <b><img src="{{ $sms }}"> SMS-авторизация  <br><br><input type='button' style="background-image: url(/img/button_bg.gif)" onclick="ChangeSecurity(2);" value='{{ $sms_bitton }}' class='button'></b><br>
												<br>
												Активировав данную опцию, при входе в систему Вам на мобильный телефон отправляется сообщение, содержащее код, необходимый для входа в систему.</td>
											</tr>
											<tr>
											  <td width="71" valign="top">&nbsp;</td>
											  <td width="529">Примечание: Отправка SMS - это платная операция. Стоимость: $0.1 USD за каждое сообщение.</td>
											</tr>
										  </table>
										</form>
										<br>
										<img src="/public/cabinet/e8.gif" width="100%" height="1"><br><a name="codecard" id="codecard"></a>
										<br>
										<form action="/user/savecodecard.asp" method="post">
										  <table width="600" border="0" cellspacing="0" cellpadding="10">
											<tr>
											  <td width="71" valign="top"><img src="/public/cabinet/codecard2.gif" /></td>
											  <? ($security->card == 0) ? $card = '/public/cabinet/off.gif' : $card = '/public/cabinet/on.gif' ?>
											  <? ($security->card == 0) ? $card_bitton = ' Включить ' : $card_bitton = ' Отключить ' ?>
											  <td width="529"> <b><img src="{{ $card }}"> Кодовая карта <br><br><input type='button' style="background-image: url(/img/button_bg.gif)" onclick="ChangeSecurity(3);" value='{{ $card_bitton }}' class='button'></b><br>
												<br>
												При активировании данной опции Вам необходимо будет вводить определенный код с кодовой карты во время операций перевода средств или вывода их из системы</td>
											</tr>
											<tr>
											  <td width="71" valign="top">&nbsp;</td>
											  <td width="529"></td>
											</tr>
										  </table>
										</form>
										<br>
										<img src="/public/cabinet/e8.gif" width="100%" height="1"><br>
										<br>
										<form action="/user/saveapi.asp" method="post">
										  <table width="600" border="0" cellspacing="0" cellpadding="10">
											<tr>
											  <td width="71" valign="top"><img src="/public/cabinet/apilogo.gif" border="0"></td>
											  <? ($security->api == 0) ? $api = '/public/cabinet/off.gif' : $api = '/public/cabinet/on.gif' ?>
											  <? ($security->api == 0) ? $api_bitton = ' Включить ' : $api_bitton = ' Отключить ' ?>
											  <td width="529"> <b><img src="{{ $api }}"> API (Автоматизированный Программный Интерфейс) <br><br><input type='button' style="background-image: url(/img/button_bg.gif)" onclick="ChangeSecurity(4);" value='{{ $api_bitton }}' class='button'></b><br>
												<img src="/public/cabinet/alerts.jpg"> &nbsp;Не включайте API на Вашем аккаунте если Вы получили заявку на его подключение от третьих лиц. Игнорирование данной рекомендации может привести к взлому Вашего аккаунта.<br><br>
												Включение данной опции активирует на вашем аккаунте интерфейс для автоматического приема платежей.</td>
											</tr>
										  </table>
										</form>
												<br>
												</div>
									  </td>
									</tr>
								  </table>
								  <br>
								  <br>
								  <table width="200" border="0" cellspacing="0" cellpadding="0">
									<tr>
									  <td width="1"><a href="{{route('cabinet')}}"><img src="/public/cabinet/bck.gif" border="0"></a></td>
									  <td>Предыдущая страница</td>
									</tr>
								  </table>
								</div>


								</div>
								</td>
									<td background="/public/cabinet/tright.gif">&nbsp;</td>
								  </tr>
								  <tr> 
									<td> 
									  <div align="right"><img src="/public/cabinet/bottomleft.gif"></div>
									</td>
									<td background="/public/cabinet/bottom.gif" colspan="2"> 
									  <div align="center"><img src="/public/cabinet/bottomleft.gif" width="1" height="1"></div>
									</td>
									<td><img src="/public/cabinet/rightbottom.gif" width="10" height="17"></td>
								  </tr>
								</table>


                            <br>


                        </td>
                    </tr>

                    <tr valign="middle">
                        <td bgcolor="#FFFFFF" valign="middle" height="12"><img src="/public/cabinet/red.gif"
                                                                               width="100%"
                                                                               height="1"></td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24" height="26"></td>
        </tr>
        </tbody>
    </table>

    <!--FOOTER-->

    @include('nav_menu.footer')
<script>

function ChangeSecurity(id)
{
	$.ajax( "{{ url('/change_security') }}", {

		type: "POST",
		data: {
			"_token": "{{ csrf_token() }}",
			id : id,
		},
		dataType: "json",

		success: function(res) {

			location.reload();

		},
		error: function() {

			alert('SERVER ERROR!!!');

		}
	});
}

</script>
@endsection