@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26">
            </td>
            <td width="94%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr background="/public/cabinet/bgg.gif">
                        <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">
                                        <script type="text/javascript">
                                            function getUserEvents() {
                                                $j.get('/user/events.asp?section=account_profile', null, function (data) {
                                                    if (data && data.length > 1) {
                                                        var params = JSON.parse(data);
                                                        $j('#actions').html(params.actions);
                                                        $j('#messages').html(params.news);
                                                    }
                                                });
                                                setTimeout('getUserEvents()', 100000);
                                            }

                                            setTimeout('getUserEvents()', 1500);
                                        </script>
                                        <noscript>
                                            <img src="/img/attention.gif" height="16" widht="16"> <font color="red"><b>Please,
                                                    enable Javascript for this site</b></font>
                                        </noscript>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table>
                                            <tbody>
                                            <tr>

                                                <td valign="top"><img
                                                            src="/public/cabinet/clock.gif">
                                                </td>
                                                <td nowrap="">Текущее время: {{\Carbon\Carbon::now()->format('d.m.y H:i')}} +0100 GMT</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img
                                                src="/public/cabinet/red.gif" width="100%"
                                                height="1"></td>
                                </tr>
                                </tbody>
                            </table>

                            @include('nav_menu.payment')

                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', 'e7e43130d6e66632d15ad985c3f7ba3f', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>


                            <br>
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td valign="top" width="585">


                                        <table width="540" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="23" valign="top"><img
                                                            src="/public/cabinet/tlefttop.gif"
                                                            width="23" height="42"></td>
                                                <td background="/public/cabinet/tbg.gif"
                                                    valign="bottom" nowrap="">
                                                    <font color="004E97" face="Arial, Helvetica, sans-serif"
                                                          size="5"><img
                                                                src="/public/cabinet/blank.gif"
                                                                width="15" height="10">
                                                        Бонус 20%
                                                    </font>
                                                </td>
                                                <td background="/public/cabinet/trighttop.gif"
                                                    width="999"><img
                                                            src="/public/cabinet/tcenter.gif"
                                                            width="60" height="42"></td>
                                                <td valign="bottom" width="10"><img
                                                            src="/public/cabinet/trr.gif"
                                                            width="10" height="42"></td>
                                            </tr>
                                            <tr>
                                                <td background="/public/cabinet/tleft.gif">
                                                    &nbsp;
                                                </td>
                                                <td colspan="2" bgcolor="#FFFFFF">
                                                    <p>&nbsp;</p>

                                                    <div style="padding: 0px 20px 0px 15px">
                                                        <div class="arabic">
                                                            На аккаунт: <font size="2" color="#CC0000">*</font>
                                                            <input type="text" value="" placeholder="U32163473">
                                                            <br>

                                                            <br>
                                                            <br>
                                                            <div id="trans">
                                                                <div class="arabic">
                                                                    <input type="button" value="Начислить">
                                                                </div>
                                                            </div>
                                                            <script type="text/javascript">
                                                                $j(document).ready(function () {
                                                                    $j.get('/user/trans.asp', null, function (data) {
                                                                        $j("#trans").html(data);
                                                                    });
                                                                });
                                                            </script>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td background="/public/cabinet/tright.gif">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div align="right"><img
                                                                src="/public/cabinet/bottomleft.gif">
                                                    </div>
                                                </td>
                                                <td background="/public/cabinet/bottom.gif"
                                                    colspan="2">
                                                    <div align="center"><img
                                                                src="/public/cabinet/bottomleft.gif"
                                                                width="1" height="1"></div>
                                                </td>
                                                <td><img
                                                            src="/public/cabinet/rightbottom.gif"
                                                            width="10" height="17"></td>
                                            </tr>
                                            </tbody>
                                        </table>


                                    </td>
                                    <td width="20"><img src="/public/cabinet/blank.gif"
                                                        width="1" height="1"></td>

                                    <td valign="top">


                                        <table width="410" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="23" valign="top"><img
                                                            src="/public/cabinet/tlefttop.gif"
                                                            width="23" height="42"></td>
                                                <td background="/public/cabinet/tbg.gif"
                                                    valign="bottom" nowrap="">
                                                    <font color="004E97" face="Arial, Helvetica, sans-serif"
                                                          size="5"><img
                                                                src="/public/cabinet/blank.gif"
                                                                width="15" height="10">
                                                        Мои счета
                                                    </font>
                                                </td>
                                                <td background="/public/cabinet/trighttop.gif"
                                                    width="999"><img
                                                            src="/public/cabinet/tcenter.gif"
                                                            width="60" height="42"></td>
                                                <td valign="bottom" width="10"><img
                                                            src="/public/cabinet/trr.gif"
                                                            width="10" height="42"></td>
                                            </tr>
                                            <tr>
                                                <td background="/public/cabinet/tleft.gif">
                                                    &nbsp;
                                                </td>
                                                <td colspan="2" bgcolor="#FFFFFF">
                                                    <p>&nbsp;</p>

                                                    <div class="arabic">
                                                        <table border="0" cellpadding="5" width="30">
                                                            <tbody>
                                                            @foreach($wallets as $wallet)
                                                                <tr>
                                                                    <td width="18" height="18"><img
                                                                                @if ($wallet->type_id == 2)
                                                                                src="/public/cabinet/U.gif"
                                                                                @elseif ($wallet->type_id == 1)
                                                                                src="/public/cabinet/G.gif"
                                                                                @else
                                                                                src="/public/cabinet/E.gif"
                                                                                @endif

                                                                                width="18" height="18">
                                                                    </td>
                                                                    <td width="45" nowrap="">
                                                                        <a href="#">{{$wallet->title}}</a>
                                                                    </td>
                                                                    <td width="16" nowrap="">
                                                                <span id="17453593"><img
                                                                            src="/public/cabinet/more.gif"
                                                                            width="16" height="14"
                                                                            style="cursor:pointer"><div
                                                                            class="tooltip fixed"
                                                                            style="display: none;"><img
                                                                                src="/public/cabinet/arrow.gif">&nbsp;&nbsp;<a
                                                                                href="#">Отправить деньги</a><img
                                                                                src="/public/cabinet/blank.gif"
                                                                                width="10" height="20"><br><img
                                                                                src="/public/cabinet/arrow.gif">&nbsp;&nbsp;<a
                                                                                href="#">Обмен</a><img
                                                                                src="/public/cabinet/blank.gif"
                                                                                width="10" height="20"><br><img
                                                                                src="/public/cabinet/arrow.gif">&nbsp;&nbsp;<a
                                                                                href="#">Смотреть выписку</a><img
                                                                                src="/public/cabinet/blank.gif"
                                                                                width="10" height="20"><br>
                                                                        <!--<hr><img src="/img/arrow.gif">&nbsp;&nbsp;<a href="javascript:;" onclick="if (copyUrl('U17453593')) alert('Account is copied to clipboard'); else alert('Your browser does not support clipboard operations'); return false">Копировать в буфер обмена</a>--></div></span>
                                                                        <script type="text/javascript">
                                                                            $j('#17453593').simpletip({
                                                                                fixed: true,
                                                                                position: ["16", "-10"],
                                                                                content: '<img src="/img/arrow.gif">&nbsp;&nbsp;<a href="/send_money.html?f=17453593">Отправить деньги</a><img src="/img/blank.gif" width="10" height="20"><br><img src="/img/arrow.gif">&nbsp;&nbsp;<a href="/exchange.html?f=17453593">Обмен</a><img src="/img/blank.gif" width="10" height="20"><br><img src="/img/arrow.gif">&nbsp;&nbsp;<a href="/account_view.html?f=17453593">Смотреть выписку</a><img src="/img/blank.gif" width="10" height="20"><br><!--<hr><img src="/img/arrow.gif">&nbsp;&nbsp;<a href="javascript:;" onclick="if (copyUrl(\'U17453593\')) alert(\'Account is copied to clipboard\'); else alert(\'Your browser does not support clipboard operations\'); return false">Копировать в буфер обмена</a>-->'
                                                                            });
                                                                        </script>
                                                                    </td>
                                                                    <td valign="bottom"><img
                                                                                src="/public/cabinet/dot.gif"
                                                                                width="95" height="5"></td>
                                                                    <td nowrap="" width="120">
                                                                        <div dir="ltr">{{$wallet->amount}} {{' ' . $wallet->type->title}}</div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            <tr>
                                                                <td width="18" height="18">&nbsp;</td>
                                                                <td nowrap="" colspan="2">
                                                                    <div align="left" style="padding: 5px 0px 0px 0px">
                                                                        <a href="#">Добавить счет</a> »
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <script type="text/javascript">
                                                        function copyUrl(account) {
                                                            if (window.clipboardData) {
                                                                window.clipboardData.setData("Text", account);
                                                                return true;
                                                            }

                                                            return false;
                                                        }
                                                    </script>

                                                </td>
                                                <td background="/public/cabinet/tright.gif">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div align="right"><img
                                                                src="/public/cabinet/bottomleft.gif">
                                                    </div>
                                                </td>
                                                <td background="/public/cabinet/bottom.gif"
                                                    colspan="2">
                                                    <div align="center"><img
                                                                src="/public/cabinet/bottomleft.gif"
                                                                width="1" height="1"></div>
                                                </td>
                                                <td><img
                                                            src="/public/cabinet/rightbottom.gif"
                                                            width="10" height="17"></td>
                                            </tr>
                                            </tbody>
                                        </table>


                                        <br><br>


                                        <table width="410" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="23" valign="top"><img
                                                            src="/public/cabinet/tlefttop.gif"
                                                            width="23" height="42"></td>
                                                <td background="/public/cabinet/tbg.gif"
                                                    valign="bottom" nowrap="">
                                                    <font color="004E97" face="Arial, Helvetica, sans-serif"
                                                          size="5"><img
                                                                src="/public/cabinet/blank.gif"
                                                                width="15" height="10">
                                                        Специальные предложения
                                                    </font>
                                                </td>
                                                <td background="/public/cabinet/trighttop.gif"
                                                    width="999"><img
                                                            src="/public/cabinet/tcenter.gif"
                                                            width="60" height="42"></td>
                                                <td valign="bottom" width="10"><img
                                                            src="/public/cabinet/trr.gif"
                                                            width="10" height="42"></td>
                                            </tr>
                                            <tr>
                                                <td background="/public/cabinet/tleft.gif">
                                                    &nbsp;
                                                </td>
                                                <td colspan="2" bgcolor="#FFFFFF">
                                                    <p>&nbsp;</p>

                                                    <center><a target="_blank" href="#"
                                                               style="color:#333333;text-decoration:none">
                                                            <div style="text-align:left;padding-left:24px;padding-right:10px;">
                                                                <h3 style="padding:0;margin:0">Нужна помощь?</h3><br>Смотрите
                                                                видеогид по работе с вебсайтом
                                                            </div>
                                                            <br><img border="0"
                                                                     src="/public/cabinet/3.jpg"></a>
                                                    </center>
                                                </td>
                                                <td background="/public/cabinet/tright.gif">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div align="right"><img
                                                                src="/public/cabinet/bottomleft.gif">
                                                    </div>
                                                </td>
                                                <td background="/public/cabinet/bottom.gif"
                                                    colspan="2">
                                                    <div align="center"><img
                                                                src="/public/cabinet/bottomleft.gif"
                                                                width="1" height="1"></div>
                                                </td>
                                                <td><img
                                                            src="/public/cabinet/rightbottom.gif"
                                                            width="10" height="17"></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                            <br>


                        </td>
                    </tr>

                    <tr valign="middle">
                        <td bgcolor="#FFFFFF" valign="middle" height="12"><img
                                    src="/public/cabinet/red.gif" width="100%" height="1">
                        </td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24"
                                                height="26"></td>
        </tr>
        </tbody>
    </table>

    <!--FOOTER-->

    @include('nav_menu.footer')

@endsection