@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

    <table width="100%" class="qwer" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26"></td>
            <td width="94%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr background="/public/cabinet/bgg.gif">
                        <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">

                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        @include('nav_menu.to_date')
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img src="/public/cabinet/red.gif" width="100%" height="1">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div id="quick_wrapper" style="background-color:#fff;min-height:60px;display:none"><br><img
                                        src="/public/cabinet/loader.gif"></div>
                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', '84fd6d164d74a2e17156686d6a0db48b', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>


                            <br>


                            <table width="750" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td width="23" valign="top"><img src="/public/cabinet/tlefttop.gif" width="23"
                                                                     height="42"></td>
                                    <td background="/public/cabinet/tbg.gif" valign="bottom" nowrap="">
                                        <font color="004E97" face="Arial, Helvetica, sans-serif" size="5"><img
                                                    src="/public/cabinet/blank.gif" width="15" height="10">
                                            Предварительный просмотр перевода
                                        </font>
                                    </td>
                                    <td background="/public/cabinet/trighttop.gif" width="999"><img
                                                src="/public/cabinet/tcenter.gif" width="60" height="42"></td>
                                    <td valign="bottom" width="10"><img src="/public/cabinet/trr.gif" width="10" height="42">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="/public/cabinet/tleft.gif">&nbsp;</td>
                                    <td colspan="2" bgcolor="#FFFFFF">
                                        <p>&nbsp;</p>

                                        <div style="padding: 0px 40px 0px 18px">
                                            <div class="arabic">
                                                <div align="right"><img src="/public/cabinet/lock4.jpg">&nbsp;&nbsp;Безопасный
                                                    Перевод&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <p><b><font face="Arial, Helvetica, sans-serif" xsize="2">Подтвердите детали
                                                            платежа, указанные ниже.<br>В случае если какая-либо информация требует
                                                            редактирования, нажмите кнопку "Редактировать";<br> если все верно, для
                                                            завершения операции нажмите кнопку "Отправить деньги".</font></b></p>
                                                <br>
                                                Пожалуйста проверьте ваш платеж для избежания ошибок. Отправленный перевод
                                                невозможно вернуть.<br>Вы согласились с политикой неотменяемости операций
                                                при регистрации аккаунта.<br>Делая эту операцию, Вы соглашаетесь, что
                                                перевод денег является окончательным и не может быть отменён.<br><br>
                                                <div style="margin-right: 10px;" class="box3">
                                                    <table border="0" cellpadding="5">
                                                        <tbody>
                                                        <tr>
                                                            <td valign="top" width="130">Сумма:</td>
                                                            <td dir="ltr"><b>{{number_format($data['amount'], 2, '.', '')}} {{' ' .  $data['type_wallet']}}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">Отправить на счет: </td>
                                                            <td dir="ltr"><b>{{$data['to_wallet']}}{{(auth()->user()->nickname_client != '' ? ('(' . auth()->user()->nickname_client . ')') : '')}}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">Тип Аккаунта:</td>
                                                            <td>
                                                                @if($status->verified_account==1)
                                                                    <font color="#2f972f">Верифицирован</font>
                                                                @else
                                                                    Не проверено
                                                                @endif

                                                                , Trust Score рейтинг: {{' '. $status->trust_score . ' '}} пунктов. <a
                                                                        href="#"
                                                                        target="_blank"><img src="/public/cabinet/tr.gif" border="0"></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">Кредитный рейтинг:
                                                            </td>
                                                            <td>Нормальный, нет задолженности по кредитам.</td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">Комиссия:</td>
                                                                @php
                                                                    $x2 = ($data['amount']*0.005)*100;
                                                                    $ost200 = $x2%100;
                                                                    $ost20 = $x2%10;

                                                                    if ($ost200 > 0){
                                                                        if($ost20>0){
                                                                            $dot3 = 2;
                                                                        }else{
                                                                            $dot3 = 1;
                                                                        }
                                                                    }else{
                                                                        $dot3 = 2;
                                                                    }
                                                                @endphp
                                                            <td dir="ltr">{{number_format(($data['amount']*0.005), $dot3, '.', '')}}{{$data['type_wallet']}}(0.50%)</td>
                                                        </tr>

                                                        <tr>
                                                            <td valign="top">Примечание:</td>
                                                            <td>{{isset($data['memo']) ? $data['memo'] : ''}}</td>
                                                        </tr>
														@if($wrn->wrn == 1)
                                                        <tr>
                                                            <td valign="top" colspan="2">&nbsp;<br></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <img src="/public/cabinet/e8.gif" width="100%"
                                                                     height="1"><br>
															</td>
														</tr>
														<tr>
															<td valign="top" colspan="2"><span style="color: red;">Внимание!</span> Вы совершаете транзакцию на счет, который был открыт менее 15 дней назад. Пожалуйста, будьте предельно осторожны.</td>
														</tr>
                                                        <tr>
                                                            <td valign="top" colspan="2">&nbsp;<br></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" colspan="2">&nbsp;<br></td>
                                                        </tr>
														@else
                                                        <tr>
                                                            <td valign="top" colspan="2">&nbsp;<br></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" colspan="2">&nbsp;<br></td>
                                                        </tr>
														@endif
                                                        <tr>
                                                            <td colspan="2">
                                                                <img src="/public/cabinet/e8.gif" width="100%"
                                                                     height="1"><br><br>
                                                                <table>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <form method="post" action="{{route('edit')}}">
                                                                                @csrf
                                                                                <input type="submit" value="Редактировать" class="button" style="border-color:#EAEAEA;background:#F8F8F8;">
                                                                            </form>
                                                                        </td>
                                                                        <td>
                                                                            <form method="post" action="{{route('run.payment')}}">
                                                                                @csrf
                                                                                &nbsp;<input type="submit" value="Отправить деньги" class="button" id="submit">
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </td>
                                    <td background="/public/cabinet/tright.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right"><img src="/public/cabinet/bottomleft.gif"></div>
                                    </td>
                                    <td background="/public/cabinet/bottom.gif" colspan="2">
                                        <div align="center"><img src="/public/cabinet/bottomleft.gif" width="1" height="1">
                                        </div>
                                    </td>
                                    <td><img src="/public/cabinet/rightbottom.gif" width="10" height="17"></td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                        </td>
                    </tr>

                    <tr valign="middle">
                        <td bgcolor="#FFFFFF" valign="middle" height="12"><img src="/public/cabinet/red.gif" width="100%"
                                                                               height="1"></td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24" height="26"></td>
        </tr>
        </tbody>
    </table>

    <!--FOOTER-->

    @include('nav_menu.footer')

@endsection