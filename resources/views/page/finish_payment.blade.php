@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

    <table width="100%" class="qwer" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26"></td>
            <td width="94%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr background="/public/cabinet/bgg.gif">
                        <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">

                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        @include('nav_menu.to_date')
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img src="/public/cabinet/red.gif" width="100%" height="1">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div id="quick_wrapper" style="background-color:#fff;min-height:60px;display:none"><br><img
                                        src="/public/cabinet/loader.gif"></div>
                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', 'cd86ec81844f3f10018742eda748a212', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>


                            <br>


                            <table width="50%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td width="23" valign="top"><img src="/public/cabinet/tlefttop.gif" width="23"
                                                                     height="42"></td>
                                    <td background="/public/cabinet/tbg.gif" valign="bottom" nowrap="">
                                        <font color="004E97" face="Arial, Helvetica, sans-serif" size="5"><img
                                                    src="/public/cabinet/blank.gif" width="15" height="10">
                                            Завершен
                                        </font>
                                    </td>
                                    <td background="/public/cabinet/trighttop.gif" width="999"><img
                                                src="/public/cabinet/tcenter.gif" width="60" height="42"></td>
                                    <td valign="bottom" width="10"><img src="/public/cabinet/trr.gif" width="10" height="42">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="/public/cabinet/tleft.gif">&nbsp;</td>
                                    <td colspan="2" bgcolor="#FFFFFF">
                                        <p>&nbsp;</p>

                                        <div style="padding: 0px 40px 0px 18px">

                                            <table width="650" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">Транзакция {{$log->amount . ' ' . $log->wallet_type->title}} на аккаунт {{' ' . $log->to_wallet . ' '}} была успешно
                                                        завершена. <br>
                                                        <br>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <div align="left"><b>Номер операции#:</b> {{$log->batch}} <br>
                                                            <b><br>
                                                                Время: </b>{{\Carbon\Carbon::parse($log->sort_date)->format('d.m.y H:i')}}
                                                        </div>
                                                        <br><br><img src="/public/cabinet/arrow.gif"> <a
                                                                href="#">Совершить
                                                            еще один перевод</a><br><br>
                                                    </td>
                                                    <td valign="top"><img src="/public/cabinet/complete.jpg" border="0"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><br>
                                                        <table width="200" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                            <tr>
                                                                <td width="1"><a
                                                                            href="#"><img
                                                                                src="/public/cabinet/bck.gif" border="0"></a></td>
                                                                <td>Возврат</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>


                                        </div>
                                    </td>
                                    <td background="/public/cabinet/tright.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right"><img src="/public/cabinet/bottomleft.gif"></div>
                                    </td>
                                    <td background="/public/cabinet/bottom.gif" colspan="2">
                                        <div align="center"><img src="/public/cabinet/bottomleft.gif" width="1" height="1">
                                        </div>
                                    </td>
                                    <td><img src="/public/cabinet/rightbottom.gif" width="10" height="17"></td>
                                </tr>
                                </tbody>
                            </table>

                            <br>


                        </td>
                    </tr>

                    <tr valign="middle">
                        <td bgcolor="#FFFFFF" valign="middle" height="12"><img src="/public/cabinet/red.gif" width="100%"
                                                                               height="1"></td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24" height="26"></td>
        </tr>
        </tbody>
    </table>

    <!--FOOTER-->

    @include('nav_menu.footer')

@endsection