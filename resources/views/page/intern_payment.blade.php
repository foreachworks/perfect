@extends('welcome')

@section('content')

    <!--HEAD-->

    @include('nav_menu.nav_cabinet')

    <!--CONTENT-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td width="38"><img src="/public/cabinet/blank.gif" width="38" height="26"></td>
          <td width="94%">

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr background="/public/cabinet/bgg.gif">
          <td colspan="2" valign="top" background="/public/cabinet/bgg.gif">

                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td height="30" valign="middle" nowrap="" width="99%">
                                        <script type="text/javascript">
                                            function getUserEvents() {
                                                $j.get('/user/events.asp?section=account_profile', null, function (data) {
                                                    if (data && data.length > 1) {
                                                        var params = JSON.parse(data);
                                                        $j('#actions').html(params.actions);
                                                        $j('#messages').html(params.news);
                                                    }
                                                });
                                                setTimeout('getUserEvents()', 100000);
                                            }

                                            setTimeout('getUserEvents()', 1500);
                                        </script>
                                        <noscript>
                                            <img src="/img/attention.gif" height="16" widht="16"> <font color="red"><b>Please,
                                                    enable Javascript for this site</b></font>
                                        </noscript>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        <table border="0" width="270" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td align="center" nowrap="">
                                                    <span id="messages"></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" valign="middle" nowrap="">
                                        @include('nav_menu.to_date')
                                    </td>
                                </tr>
                                <tr valign="top" bgcolor="#FFFFFF">
                                    <td colspan="3" height="6"><img
                                                src="/public/cabinet/red.gif" width="100%"
                                                height="1"></td>
                                </tr>
                                </tbody>
                            </table>

                            <script type="text/javascript">
                                function upload_quick() {
                                    $j('#quick_wrapper').load('/send/quick.asp', function () {
                                        $j('#send_form').submit(function (event) {
                                            event.preventDefault();

                                            //$j('#quick_submit_wrapper').html('<img src="/img/loader.gif" style="vertical-align:text-bottom;margin-top:4px;">');

                                            $button = $j('#quick_sbt');

                                            $j.cookie('send_cookie', 'e7e43130d6e66632d15ad985c3f7ba3f', {path: '/'});

                                            $j.ajax({
                                                type: 'POST',
                                                url: '/send/quick.asp',
                                                data: $j(this).serialize(),
                                                success: function (data) {
                                                    if (data != 'preview')
                                                        upload_quick();
                                                    else
                                                        document.location = '/send_preview.html';
                                                }
                                            });

                                            $button.attr('disabled', true);
                                        });
                                    });
                                };

                                jQuery(document).ready(function () {
                                    $j('#quick_payment_button').click(function () {
                                        if ($j('#quick_wrapper').is(':visible')) {
                                            $j('#quick_wrapper').hide();
                                            $j('#quick_wrapper').html('<br><img src="/img/loader.gif">');
                                        } else {
                                            $j('#quick_wrapper').show();
                                            upload_quick();
                                        }
                                    });
                                });
                            </script>


<br>



<table width="1100" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr> 
    <td width="23" valign="top"><img src="/public/cabinet/tlefttop.gif" width="23" height="42"></td>
    <td background="/public/cabinet/tbg.gif" valign="bottom" nowrap="">
<font color="004E97" face="Arial, Helvetica, sans-serif" size="5"><img src="/public/cabinet/blank.gif" width="15" height="10">
Послать деньги или e-Metal
</font>
</td>
    <td background="/public/cabinet/trighttop.gif" width="999"><img src="/public/cabinet/tcenter.gif" width="60" height="42"></td>
    <td valign="bottom" width="10"><img src="/public/cabinet/trr.gif" width="10" height="42"></td>
  </tr>
  <tr> 
    <td background="/public/cabinet/tleft.gif">&nbsp;</td>
    <td colspan="2" bgcolor="#FFFFFF"> 
<p>&nbsp;</p>      

<div style="padding: 0px 40px 0px 20px">
<br>
<div class="arabic"> Отправляйте деньги кому угодно внутри системы Perfect Money. Делайте периодические платежи или просто платеж, выбрав счет, с которого вы хотите отправить средства.
  <p>Минимальная сумма перевода 0.01 USD или EUR в валюте или 10 USD золотом.<br>
    <br>
    <br>
    <b><font size="3">Выберите счет для отправки денежного перевода</font><br>
    <br>
    </b>
  <table border="0" cellpadding="5">
	  <tbody>
		@foreach($wallets as $wallet)
			@php
				$x1 = $wallet->amount*100;
				$ost100 = $x1%100;
				$ost10 = $x1%10;

				if ($ost100 > 0){
					if($ost10>0){
						$dot2 = 2;
					}else{
						$dot2 = 1;
					}
				}else{
					$dot2 = 2;
				}
			@endphp
		    <tr>
			  <td width="18" height="18" valign="middle"><img
				@if ($wallet->type_id == 2)
				src="/public/cabinet/U.gif"
				@elseif ($wallet->type_id == 1)
				src="/public/cabinet/G.gif"
				@else
				src="/public/cabinet/E.gif"
				@endif

				width="18" height="18">
			  </td>
		      <td nowrap="" width="100" valign="middle"><b><font style="font-size:15px">{{$wallet->title}}</font></b></td>
			  <td dir="ltr" nowrap="" valign="middle"><font style="font-size:14px">{{number_format($wallet->amount, $dot2, '.', '')}} {{' ' . $wallet->type->title}} </font></td>
			  @if($wallet->amount > 0)
			  <td nowrap valign="middle">
				<div align="center"><img src="/public/cabinet/single.gif" width="23" height="17" border="0" style="vertical-align: middle;"> <a href="{{route('send_intern', ['segment2' => $wallet->id])}}">Единичный платеж</a></div>
			  </td>
			  <td nowrap valign="middle">
				<div align="center"><img src="/public/cabinet/to_mail.gif" width="26" height="26" border="0" style="vertical-align: middle;"> <a href="#">Платеж на e-mail</a></div>
			  </td>
			  <td nowrap valign="middle">
				<div align="center"><img src="/public/cabinet/to_sms.gif" width="25" height="25" border="0" style="vertical-align: middle;"> <a href="#">Платеж на мобильный телефон</a></div>
			  </td>
			  <td nowrap valign="middle">
				<div align="center"><img src="/public/cabinet/recurrent.gif" width="25" height="25" border="0" style="vertical-align: middle;"> <a href="#">Платеж по расписанию</a></div>
			  </td>
			  @endif
			  <td nowrap="" valign="middle">
				<div align="center"></div>
			  </td>
					<td width="10">
			  </td>
			</tr>
			<tr>
			  <td colspan="9"><img src="/public/cabinet/red.gif" width="100%" height="1"></td>
			</tr>
		@endforeach	
	  </tbody>
  </table>
</p></div>
&nbsp;&nbsp;<a href="/payee_send.html">Список получателей платежей</a> »<br>
<br><br>
<table width="200" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td width="1"><a href="javascript:history.go(-1)"><img src="/public/cabinet/bck.gif" border="0"></a></td>
    <td>Предыдущая страница</td>
    <td>&nbsp;</td>
  </tr>
</tbody></table>


</div>
</td>
    <td background="/public/cabinet/tright.gif">&nbsp;</td>
  </tr>
  <tr> 
    <td> 
      <div align="right"><img src="/public/cabinet/bottomleft.gif"></div>
    </td>
    <td background="/public/cabinet/bottom.gif" colspan="2"> 
      <div align="center"><img src="/public/cabinet/bottomleft.gif" width="1" height="1"></div>
    </td>
    <td><img src="/public/cabinet/rightbottom.gif" width="10" height="17"></td>
  </tr>
</tbody></table>

<br>


  </td>
        </tr>

        <tr valign="middle">
          <td bgcolor="#FFFFFF" valign="middle" height="12"><img src="/public/cabinet/red.gif" width="100%" height="1"></td>
        </tr>


      </tbody></table>
          </td>
          <td width="3%" valign="middle"><img src="/public/cabinet/blank.gif" width="24" height="26"></td>
        </tr>
      </tbody></table>
    <!--FOOTER-->

    @include('nav_menu.footer')

@endsection