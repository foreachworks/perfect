<div id="my_quick_wrapper" style="display:none; background-color: rgb(255, 255, 255); min-height: 60px;">
    <form method="post" action="{{route('payment')}}" id="send_form">
        @csrf
        <table>
            <tbody>
            <tr>
                <td valign="top" nowrap="">С аккаунта: <font size="2" color="#CC0000">*</font>
                </td>
                <td valign="top" nowrap="">На аккаунт: <font size="2" color="#CC0000">*</font>
                </td>
                <td valign="top" nowrap="">Сумма<span id="currency_quick"></span>: <font
                            size="2" color="#CC0000">*</font></td>
            </tr>
            <tr>
                <td valign="top">
                    <div style="padding: 0px 20px 0px 0px">
                        <select onchange="FuncUsd();" name="from_wallet" id="account_quick" style="font-size: 13px; font-family: Tahoma; border: 1px solid #004E97; background-color: #F0F8FF;">
                            <option></option>
                            @foreach($wallets as $wallet)
                                <option value="{{$wallet->id}}">{{$wallet->title . ', Сумма : ' . number_format($wallet->amount, 2, '.', '') . ' ' . $wallet->type->title}}</option>
                            @endforeach
                        </select>
                        <div class="error"></div>
                    </div>
                </td>
                <td valign="top">
                    <div style="padding: 0px 20px 0px 0px">
                        <input type="text" name="to_wallet" size="20" maxlength="20" style="font-size: 13px; font-family: Tahoma; border: 1px solid #004E97; background-color: #F0F8FF;" autocomplete="off"> <a href="#"><img
                                    src="/public/cabinet/payee_list.png"
                                    title="Показать список получателей"
                                    style="border:0;vertical-align:text-bottom; float: right;"></a>

                        <div class="error"></div>
                    </div>
                </td>
                <td valign="top">
                    <div style="padding: 0px 0px 0px 0px">
                        <input type="text"name="amount" size="20" maxlength="20" value="" autocomplete="off">
                        <span id="quick_submit_wrapper">
		<input type="submit" value="Предпросмотр платежа" class="button" id="quick_sbt">
		</span>
                        <div class="error" id="balance_error"></div>
                    </div>
                </td>
                <td nowrap="" style="opacity:0.7;filter:alpha(Opacity=70);" valign="top">
                    <div style="padding: 2px 0px 0px 20px"><img src="/public/cabinet/lock4.jpg"
                                                                style="float: left; vertical-align:text-bottom">&nbsp;&nbsp;Безопасный
                        Перевод
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    <br>

</div>
<script>

	function FuncUsd()
	{
		$('#currency_quick').html(' (USD)');
	}
	
</script>