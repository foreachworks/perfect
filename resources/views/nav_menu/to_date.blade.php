<table>
    <tbody>
    <tr>
        @if(auth()->user()->verified_account==1)
            <td>
                <img src="/public/cabinet/success.png">&nbsp;
            </td>
            <td nowrap="">Верифицированный аккаунт</td>
        @endif
        <td valign="top">&nbsp;&nbsp;<img src="/public/cabinet/clock.gif">
        </td>
        <td nowrap="">Текущее время: {{\Carbon\Carbon::now()->subMinute(config('app.sub_minute'))->format('d.m.y H:i')}} +0100 GMT
        </td>
    </tr>
    </tbody>
</table>