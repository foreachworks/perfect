<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Perfect Money – новое поколение платежных систем Интернета. Платежная система для денежных переводов</title>
<META NAME="Keywords" CONTENT="купить,продать,поменять,онлайн,евалюта,электронная валюта,платежная система, пеймент процессор, платежный шлюз,api мерчант,merchant, оплата,средство,онлайн банкинг,денежный перевод,финансовый сервис,платежный сервис,швейцарская,безопасность, деньги">
<META name="description" content="Платежная система Perfect Money открывает самый простой и безопасный финансовый сервис для совершения денежных переводов по всему миру.Принимайте электронную валюту, банковские переводы и смс платежи на Вашем вебсайте.Покупайте золото, отправляйте или принимайте деньги с наиболее безопасной платежной системой в Интернете">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
body { max-width:1650px}
.top {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8pt}
.req {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9pt; color: #FF0000}
td {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: #333333}
.ag {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #333333}
.menu {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt}
.txt {  text-align: justify}
a {  color: #990000}
.boxalert{
	border: 1px solid #DDDDDD;
	color: red;
	font-weight: bold;
	padding: 10px;
	background-color: #f2f2f2;
}
#master-content TABLE.mark TD
{
padding: 3px 2px;
border-bottom: 1px dotted #CCCCCC;
border-top: 1px dotted #CCCCCC;
}

#master-content TABLE.mark TH
{
border-bottom: 1px dotted #CCCCCC;
}
</style>
<link rel="StyleSheet" href="/public/home/style.css" type="text/css">
<link rel="StyleSheet" href="/public/home/colorbox_publics.css" type="text/css">
<script type="text/javascript" src="/public/home/jquery.comp.js"></script>
<script type="text/javascript">
$j = jQuery.noConflict();	
jQuery(document).ready(function(){  
	$j("#memo").addClass("input");
	$j("input").addClass("input");
	$j(":submit").addClass("submit");
});
</script>
<script type="text/javascript" src="/public/home/jquery.1.9.min.js"></script>
<script type="text/javascript" src="/public/home/jquery.colorbox-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){  
	$('a[href="#"]').colorbox({
		href: "/tour.html",
		scrolling: false,
		initialWidth: 1000,
		initialHeight: 590,
		innerWidth: 977,
		width: 1000,
		height: 590,
		fixed: true,
		top: 60,
		left: 350
	});
});
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="general">

@yield('content')

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
    <tr>
        <td width="341" bgcolor="#ffffff" valign="middle" height="56">
            <div align="left">&nbsp;&nbsp;
                <!--
                <a href="https://itunes.apple.com/gb/app/perfect-money/id653398845?mt=8" target="_blank"><img src="/img/mobile/ios_app_store.png" width="155" height="54"></a>
                <a href="https://play.google.com/store/apps/details?id=com.touchin.perfectmoney&hl=en" target="_blank"><img src="/img/mobile/googleplayicon.png" width="155" height="54"></a>
                -->

            </div>
        </td>
        <td bgcolor="#ffffff" valign="top" height="56">
            <table width="100%" border="0" cellspacing="5" cellpadding="5" align="right">
                <tbody>
                <tr>
                    <td>
                        <div align="right">
                            <small><font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                    Perfect Money – новое поколение платежных систем Интернета. Платежная система для
                                    денежных переводов&nbsp;<br>©
                                    2007-2021 SR &amp; I. All rights reserved.&nbsp;<br>
                                    <a href="#"><font color="#b50b0b">Партнерская
                                            программа</font></a>
                                    | <a href="#"><font color="#b50b0b">Perfect
                                            Money API</font></a> | <a href="#"><font
                                                color="#b50b0b">Юридический аспект</font></a>
                                    | <a href="#"><font color="#b50b0b">Правовое
                                            положение</font></a>
                                    <small><font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                            | <a href="#"><font color="#b50b0b">Условия
                                                    использования</font></a></font></small>
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                        | <a href="#"><font
                                                    color="#b50b0b">AML</font></a></font></font></small>
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1"><br><font
                                        face="Verdana, Arial, Helvetica, sans-serif" size="1"><a
                                            href="#"><font color="#b50b0b">Карта
                                            сайта</font></a></font></font>

                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<div id="cboxOverlay" style="display: none;"></div>
<div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
    <div id="cboxWrapper">
        <div>
            <div id="cboxTopLeft" style="float: left;"></div>
            <div id="cboxTopCenter" style="float: left;"></div>
            <div id="cboxTopRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="cboxMiddleLeft" style="float: left;"></div>
            <div id="cboxContent" style="float: left;">
                <div id="cboxTitle" style="float: left;"></div>
                <div id="cboxCurrent" style="float: left;"></div>
                <button type="button" id="cboxPrevious"></button>
                <button type="button" id="cboxNext"></button>
                <button type="button" id="cboxSlideshow"></button>
                <div id="cboxLoadingOverlay" style="float: left;"></div>
                <div id="cboxLoadingGraphic" style="float: left;"></div>
            </div>
            <div id="cboxMiddleRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="cboxBottomLeft" style="float: left;"></div>
            <div id="cboxBottomCenter" style="float: left;"></div>
            <div id="cboxBottomRight" style="float: left;"></div>
        </div>
    </div>
    <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>
</div>
</body>
</html>
