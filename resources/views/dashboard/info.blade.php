@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <table class="table table-hover table-dark">
                            <thead>
                            <tr>
                                <th scope="col">First name</th>
                                <th scope="col">Last name</th>
                                <th scope="col">Father name</th>
                                <th scope="col">Business account</th>
                                <th scope="col">Verified account</th>
                                <th scope="col">Trust score</th>
                                <th scope="col">Nickname client</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->father_name}}</td>
                                <td>{{($user->business_account==1) ? 'Active' : 'Disabled'}}</td>
                                <td>{{($user->verified_account==1) ? 'Active' : 'Disabled'}}</td>
                                <td>{{$user->trust_score}}</td>
                                <td>{{$user->nickname_client}}</td>
                                <td><a class="btn btn-warning"
                                       href="{{route('info.edit', ['id' => $user->id])}}">Edit</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

					<div class="form-group col-md-12">
						<div class="custom-control custom-checkbox" style="padding-left: 0.4rem;">
							<input type="checkbox" name="wrn" id="wrn" onclick="EditWrn();" {{(int)$wrn_status->wrn == 1 ? 'checked' : ''}}>
							<label>Активировать предостережение</label>
						</div>
					</div>
					
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

function EditWrn()
{
	$.ajax( "{{ action('DashboardController@updateWrn') }}", {
		type: "POST",
		data: {
			
		},
		dataType: "json",
		
		success: function(res) {

			alert('Изменения успешно сохранены!');

		},
		error: function (error) {
				
			alert('SERVER ERROR!');
			
		}
	});
}

</script>