@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <form method="post" action="{{route('update.info', ['id' => $user->id])}}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="inputCity">First name</label>
                                    <input type="text" value="{{$user->first_name}}"
                                           class="form-control" name="first_name" id="inputCity" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputCity">Last name</label>
                                    <input type="text" value="{{$user->last_name}}"
                                           class="form-control" name="last_name" id="inputCity" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputCity">Father name</label>
                                    <input type="text" value="{{$user->father_name}}"
                                           class="form-control" name="father_name" id="inputCity" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputCity">Trust score</label>
                                    <input type="text" value="{{$user->trust_score}}"
                                           class="form-control" name="trust_score" id="inputCity" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <div class="custom-control custom-checkbox" style="padding-left: 0rem;">
                                        <input type="checkbox" name="business_account"
                                               value="1" {{isset($user) ? (($user->business_account==1) ? 'checked' : '') : ''}} >
                                        <label>Business account</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="custom-control custom-checkbox" style="padding-left: 0rem;">
                                        <input type="checkbox" name="verified_account"
                                               value="1" {{isset($user) ? (($user->verified_account==1) ? 'checked' : '') : ''}} >
                                        <label>Verified account</label>
                                    </div>
                                </div>
							</div>	
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="inputCity">Nickname client</label>
                                    <input type="text" value="{{$user->nickname_client}}"
                                           class="form-control" name="nickname_client" id="inputCity" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputCity">Trust score</label>
                                    <input type="text" value="{{$status->trust_score}}"
                                           class="form-control" name="trust_score_w" id="inputCity" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <div class="custom-control custom-checkbox" style="padding-left: 0rem;">
                                        <input type="checkbox" name="business_account_w"
                                               value="1" {{isset($status) ? (($status->business_account==1) ? 'checked' : '') : ''}} >
                                        <label>Business account</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="custom-control custom-checkbox" style="padding-left: 0rem;">
                                        <input type="checkbox" name="verified_account_w"
                                               value="1" {{isset($status) ? (($status->verified_account==1) ? 'checked' : '') : ''}} >
                                        <label>Verified account</label>
                                    </div>
                                </div>
							</div>	

                            <input type="submit" value="Save">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
