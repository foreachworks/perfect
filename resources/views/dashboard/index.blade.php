@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <table class="table table-hover table-dark">
                            <thead>
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Type</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Status</th>
                                <th scope="col" colspan="2">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($wallets as $wallet)
                                <tr>
                                    <td>{{$wallet->title}}</td>
                                    <td>{{$wallet->type->title}}</td>
                                    <td>{{$wallet->amount}}</td>
                                    <td>{{($wallet->status==1) ? 'Active' : 'Disabled'}}</td>
                                    <td><a class="btn btn-warning" href="{{route('wallet.edit', ['id' => $wallet->id])}}">Edit</a></td>
                                    <td>
                                        <form id="delete_form_project{{$wallet->id}}" method="post"
                                              action="{{route('wallet.destroy', ['wallet' => $wallet->id])}}">
                                            @method('DELETE')
                                            @csrf
                                            <a class="btn btn-danger" style="cursor: pointer"
                                               onclick="document.getElementById('delete_form_project{{$wallet->id}}').submit()">Delete</a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
