@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <form method="post"
                              action="{{isset($wallet) ? route('wallet.update', ['id' => $wallet->id]) : route('wallet.store')}}">
                            @csrf
                            @if(isset($wallet))
                                @method('put')
                            @endif
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputCity">Number</label>
                                    <input type="text" value="{{isset($wallet) ? $wallet->title : ''}}" maxlength="9"
                                           minlength="9" class="form-control" name="title" id="inputCity" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputState">Type</label>
                                    <select id="inputState" name="type" class="form-control" required>
                                        <option selected value="{{null}}">Choose...</option>
                                        @foreach($type as $id => $title)
                                            <option value="{{$id}}" {{isset($wallet) ? (($wallet->type_id==$id) ? 'selected' : '') : ""}}>{{$title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputZip">Amount</label>
                                    <input type="text" value="{{isset($wallet) ? $wallet->amount : ''}}" name="amount"
                                           class="form-control" id="inputZip" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status" value="{{isset($wallet) ? $wallet->status : 0}}" {{isset($wallet) ? (($wallet->status==1) ? 'checked' : '') : ''}} >
                                        <label>Status</label>
                                    </div>
                                </div>
                            </div>

                            <input type="submit" value="Save">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
