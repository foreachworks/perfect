@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <div class="card">
                    <div class="card-header">Your wallets</div>

                    <div class="card-body">
                        <ul>
                            @foreach($wallets as $wallet)
                                <li>{{$wallet->title . ' - ' . $wallet->amount}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <form method="post"
                              action="{{route('store.range')}}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputCity">Action</label>
                                    <select id="inputState" name="action_id" class="form-control" required>
                                        <option value="{{null}}"></option>
                                        @foreach($action as $id => $title)
                                            <option value="{{$id}}">{{$title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">From Wallet</label>
                                    {{--<select id="inputState" name="from_wallet" class="form-control" required>--}}
                                        {{--<option value="{{null}}"></option>--}}
                                        {{--@foreach($wallets as $wallet)--}}
                                            {{--<option value="{{$wallet->title}}">{{$wallet->title . ' - '}}{{$wallet->amount}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    <input type="text" value="" name="from_wallet" placeholder="U76334113"
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">To Wallet</label>
                                    <select id="inputState" name="to_wallet" class="form-control" required>
                                        <option value="{{null}}"></option>
                                        @foreach($wallets as $wallet)
                                            <option value="{{$wallet->title}}">{{$wallet->title . ' - '}}{{$wallet->amount}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputState">Amount</label>
                                    <input type="text" value="" name="amount" placeholder="12132456"
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputZip">Memo</label>
                                    <input type="text" value="" name="memo" placeholder="Robo-global"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="inputZip">Batch</label>
                                    <input type="text" value="" name="batch" placeholder="12132456"
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">Step batch</label>
                                    <input type="text" value="" placeholder="100"
                                           name="step_batch"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputZip">From date</label>
                                    <input type="text" name="form_date" value="{{isset($log) ? \Carbon\Carbon::parse($log->sort_date)->format('d.m.Y H:i') : ''}}" size="10" class="xcalend custom_style_calendar" autocomplete="off"  onClick="xCal(this)" onmouseenter="xCal(this)" onKeyUp="xCal()">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputZip">To date</label>
                                    <input type="text" name="to_date" value="{{isset($log) ? \Carbon\Carbon::parse($log->sort_date)->format('d.m.Y H:i') : ''}}" size="10" class="xcalend custom_style_calendar" autocomplete="off"  onClick="xCal(this)" onmouseenter="xCal(this)" onKeyUp="xCal()">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputZip">From hour</label>
                                    <input type="text" placeholder="12" name="from_hour">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputZip">To hour</label>
                                    <input type="text" placeholder="15" name="to_hour">
                                </div>
                            </div>
                            <input type="submit" value="Save">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        xCal("date8", {
            id: "cssworldru8", // Задать уникальный ID
            "class": "xcalend", // CSS класс оформления внешнего вида
            hide: 1, // Не скрывать календарь
            x: 0, // Отключить кнопку закрытия календаря
            autoOff: 0, // Отключить автоматическое скрытие
            to: "date8", // Разместить календарь внутри элемента с id=date8
            fn: "alert" // Вызвать функцию с указанным названием, в нее будет передан результат выбора
        });
    </script>

@endsection
