@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <div class="card">
                    <div class="card-header">Your wallets</div>

                    <div class="card-body">
                        <ul>
                            @foreach($wallets as $wallet)
                                <li>{{$wallet->title . ' - ' . $wallet->amount}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <form method="post"
                              action="{{isset($log) ? route('log.update', ['id' => $log->id]) : route('log.store')}}">
                            @csrf
                            @if(isset($log))
                                @method('put')
                            @endif
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputCity">Action</label>
                                    <select id="inputState" name="action_id" class="form-control" required>
                                        <option value="{{null}}"></option>
                                        @foreach($action as $id => $title)
                                            <option value="{{$id}}" {{isset($log) ? !is_null($log->action_id) ? $log->action_id==$id ? 'selected' : '' : '' : ''}}>{{$title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputState">Type</label>
                                    <select id="inputState" name="type_id" class="form-control" required>
                                        <option value="{{null}}"></option>
                                        @foreach($type as $id => $title)
                                            <option value="{{$id}}" {{isset($log) ? !is_null($log->type_id) ? $log->type_id==$id ? 'selected' : '' : '' : ''}}>{{$title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">Amount</label>
                                    <input type="text" value="{{isset($log) ? $log->amount : ''}}" name="amount"
                                           class="form-control" id="inputZip">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputZip">From Wallet</label>
                                    <input type="text" value="{{isset($log) ? $log->from_wallet : ''}}"
                                           name="from_wallet"
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">To Wallet</label>
                                    <input type="text" value="{{isset($log) ? $log->to_wallet : ''}}" name="to_wallet"
                                           class="form-control" autocomplete="off">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputState">Wallet type</label>
                                    <select id="inputState" name="wallet_type_id" class="form-control">
                                        <option value="{{null}}"></option>
                                        @foreach($walletType as $id => $title)
                                            <option value="{{$id}}" {{isset($log) ? !is_null($log->wallet_type_id) ? $log->wallet_type_id==$id ? 'selected' : '' : '' : ''}}>{{$title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputZip">Memo</label>
                                    <input type="text" value="{{isset($log) ? $log->memo : ''}}" name="memo"
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">Batch</label>
                                    <input type="text" value="{{isset($log) ? $log->batch : ''}}" name="batch"
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">Ip</label>
                                    <input type="text" value="{{isset($log) ? $log->ip : ''}}" placeholder="95.63.*.*"
                                           name="ip"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status"
                                               value="1" {{isset($log) ? (($log->status==1) ? 'checked' : '') : ''}} >
                                        <label>Status</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputZip">Date</label>
                                    {{--<input type="datetime-local" name="sort_date">--}}
                                    <input type="text" name="sort_date" value="{{isset($log) ? \Carbon\Carbon::parse($log->sort_date)->format('d.m.Y') : ''}}" size="10" class="xcalend custom_style_calendar"  onClick="xCal(this)" onmouseenter="xCal(this)" onKeyUp="xCal()">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputZip">Time</label>
                                    {{--<input type="datetime-local" name="sort_date">--}}
                                    <input type="text" name="sort_time" value="{{isset($log) ? \Carbon\Carbon::parse($log->sort_date)->format('H:i') : ''}}" placeholder="15:32" size="10">
                                </div>
                            </div>

                            <input type="submit" value="Save">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        xCal("date8", {
            id: "cssworldru8", // Задать уникальный ID
            "class": "xcalend", // CSS класс оформления внешнего вида
            hide: 1, // Не скрывать календарь
            x: 0, // Отключить кнопку закрытия календаря
            autoOff: 0, // Отключить автоматическое скрытие
            to: "date8", // Разместить календарь внутри элемента с id=date8
            fn: "alert" // Вызвать функцию с указанным названием, в нее будет передан результат выбора
        });
    </script>
@endsection
