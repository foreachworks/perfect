@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <table class="table table-hover table-dark">
                            <thead>
                            <tr>
                                <th scope="col">Type</th>
                                <th scope="col">Action</th>
                                <th scope="col">From wallet</th>
                                <th scope="col">To wallet</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Wallet type</th>
                                <th scope="col">Batch</th>
                                <th scope="col">Memo</th>
                                <th scope="col">Ip</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                <th scope="col" colspan="2">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($logs as $log)
                                <tr>
                                    <td>{{$log->type->title}}</td>
                                    <td>{{$log->action->title}}</td>
                                    <td>{{$log->from_wallet}}</td>
                                    <td>{{$log->to_wallet}}</td>
                                    <td>{{$log->amount}}</td>
                                    <td>{{!is_null($log->wallet_type) ? $log->wallet_type->title : ""}}</td>
                                    <td>{{$log->batch}}</td>
                                    <td>{{$log->memo}}</td>
                                    <td>{{$log->ip}}</td>
                                    <td>{{$log->sort_date}}</td>
                                    <td>{{$log->status ? 'Active' : 'Disabled'}}</td>
                                    <td><a class="btn btn-warning" href="{{route('log.edit', ['id' => $log->id])}}">Edit</a>
                                    <td>
                                        <form id="delete_form_log{{$log->id}}" method="post"
                                              action="{{route('log.destroy', ['log' => $log->id])}}">
                                            @method('DELETE')
                                            @csrf
                                            <a class="btn btn-danger" style="cursor: pointer"
                                               onclick="document.getElementById('delete_form_log{{$log->id}}').submit()">Delete</a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection