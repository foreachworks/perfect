@extends('welcome')

@section('content')

{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('login') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<div class="form-check">--}}
                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                    {{--<label class="form-check-label" for="remember">--}}
                                        {{--{{ __('Remember Me') }}--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-8 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Login') }}--}}
                                {{--</button>--}}

                                {{--@if (Route::has('password.request'))--}}
                                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                        {{--{{ __('Forgot Your Password?') }}--}}
                                    {{--</a>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
    <tr>
        <td height="43"><br><img alt="Payment System Gold USD EUR"
                                 src="/public/home/blank.gif"
                                 width="950" height="5">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="35%"><img alt="E-Currency Payment System"
                                         src="/public/home/blank.gif"
                                         width="14" height="26"><a href="{{route('home')}}"><img
                                    alt="Perfect Money Payment System"
                                    src="/public/home/logo3.png"
                                    border="0"></a></td>
                    <td valign="bottom" width="65%">
                        <div align="right">
                            <form method="post" name="f" action="https://perfectmoney.is/general/lang.asp">
                                <table cellpadding="0" cellspacing="0">
                                    <tbody>
                                    <tr>
                                        <td valign="middle"><img
                                                    src="/public/home/RU.GIF">&nbsp;<br><br>
                                        </td>
                                        <td><select name="lang"
                                                    onchange="if (this.value != &#39;&#39;) document.f.submit()">&nbsp;&nbsp;<option
                                                        value=""> --- ---
                                                </option>
                                                <option value="en_US">English</option>
                                                <option value="de_DE">Deutsch</option>
                                                <option value="el_GR">Ελληνικά</option>
                                                <option value="zh_CN">中文</option>
                                                <option value="ja_JP">日本語</option>
                                                <option value="ko_KR">한국어</option>
                                                <option value="es_ES">Español</option>
                                                <option value="fr_FR">Français</option>
                                                <option value="ru_RU" selected="">Русский</option>
                                                <option value="uk_UA">Українська</option>
                                                <option value="it_IT">Italiano</option>
                                                <option value="pt_PT">Português</option>
                                                <option value="ar_AE">العربية</option>
                                                <option value="fa_IR">فارسی</option>
                                                <option value="th_TH">ไทย</option>
                                                <option value="id_ID">Indonesia</option>
                                                <option value="ms_MY">Malaysian</option>
                                                <option value="tr_TR">Türkçe</option>
                                                <option value="pl_PL">Polski</option>
                                                <option value="ro_RO">Român</option>
                                                <option value="hi_IN">Hindi</option>
                                                <option value="ur_IN">Urdu</option>
                                                <option value="vi_VN">Vietnam</option>&nbsp;&nbsp;</select><img
                                                    alt="Switzerland E-Currency"
                                                    src="/public/home/blank.gif"
                                                    width="42" height="5"><br>
                                            <img alt="Switzerland E-Currency"
                                                 src="/public/home/blank.gif"
                                                 width="1" height="15"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>

                            <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
                                @if (Route::has('login'))
                                        @auth
                                            <a href="{{route('cabinet')}}"><font color="#004e97">Мой
                                                    аккаунт</font></a>
                                        @else
                                            @if (Route::has('register'))
                                                <a href="{{ route('register') }}"><font
                                                            color="#000000">Регистрация</font></a>&nbsp;&nbsp;&nbsp;
                                            @endif
                                            <font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
                                                    href="{{ route('login') }}"><font color="#000000">Вход</font></a>
                                            &nbsp;&nbsp;&nbsp;
                                            <font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
                                                    href="https://perfectmoney.is/business-partners.html"><font
                                                        color="#000000">Обменные
                                                    пункты</font></a>
                                        @endauth
                                @endif
                                &nbsp;&nbsp;&nbsp;<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
                                    <font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
                                            href="https://perfectmoney.is/tour.html" class="cboxElement"><font
                                                color="#000000">Тур</font></a>&nbsp;&nbsp;&nbsp;
                                    <font color="#999999"></font></font><font color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
                                        href="https://perfectmoney.is/help.html"><font color="#000000">Помощь</font></a>&nbsp;&nbsp;&nbsp;<font
                                        color="#999999">|</font>&nbsp;&nbsp;&nbsp;<a
                                        href="https://perfectmoney.is/security_center.html"><font color="#000000">Центр
                                        безопасности</font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>

                            </font>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
        </td>
    </tr>
    </tbody>
</table>
<!--
<div id="season" style="position:absolute; width:1px; height:1px; z-index:1; left: 285px; top: 17px"><img src="/public/home/season1_1.png"></div>
-->
<div id="season" style="position:absolute; width:1px; height:1px; z-index:-1; left: 356px; top: 5px"><img src="/public/home/summer4.gif"></div>

<table width="98%" style="margin: auto 1%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="0B0A0C">
                <tbody>
                <tr>
                    <td height="8" width="2%"><img alt="Buy Gold Metal, Buy USD EURO currency online, Payment System"
                                                   src="/public/home/top2-70.png">
                    </td>
                    <td height="8" bgcolor="#0A0A0A">
                        <div align="center">
                            <table width="216" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td><img alt="E-Currency Payment System"
                                             src="/public/home/mid3-70.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td height="8" width="1%">
                        <div align="right"><img alt="fast,easy,comfortable - way to develop your money"
                                                src="/public/home/right2-70.png"
                                                width="18" height="167"></div>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="36"><img style="display: block;"
                                src="/public/home/left-70.gif"
                                width="36" height="26"></td>
                    <td colspan="2" bgcolor="B01111">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td width="100">&nbsp;<a class="no" href="{{route('login')}}"><font
                                                face="Verdana, Arial, Helvetica, sans-serif" size="2"
                                                color="#FFFFFF">Вход</font></a>
                                </td>
                                <td width="150"><img
                                            src="/public/home/blank.gif"
                                            width="150" height="10"></td>
                                <td nowrap="">
                                    <div id="menuOver">
                                        <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
                                            <div class="menu" style="min-width:780px"><a
                                                        href="{{route('home')}}"><span>Главная</span></a>
                                                <a href="#" class="selectedd"><span>О Нас</span></a>
                                                <a href="#"><span>Возможности</span></a>
                                                <a href="#"><span>Комиссии</span></a>
                                                <a href="#"><span>E-Ваучеры</span></a>
                                                <a href="#"><span>Гарантии</span></a>
                                                <a href="#"><span>F.A.Q.</span></a>
                                                <a href="#"><span>Обратная Связь</span></a>
                                            </div>
                                        </font>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="4%" bgcolor="B01111" style="border-bottom-right-radius: 7px" valign="middle">
                        <div align="right"></div>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td><img
                    src="/public/home/blank.gif"
                    width="820" height="1"></td>
    </tr>
    </tbody>
</table>

<meta name="apple-itunes-app" content="app-id=id653398845">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td width="23"><img
                    src="/public/home/blank.gif"
                    width="23" height="26"></td>
        <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="310" valign="top"
                        background="/public/home/left33.gif">
                        <div align="left"><font face="Arial, Helvetica, sans-serif" size="3"><b><br>
                                    Полезные советы</b></font><font face="Arial, Helvetica, sans-serif" size="3"><b></b></font>
                            <br>
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF0000"><b>
                                    <br>
                                </b></font>
                            <div align="left">
                                <table width="250" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td><strong>Вход в систему:</strong> В поле ID необходимо ввести комбинацию цифр
                                            (ID клиента), которая была выслана Вам на указанный Вами адрес электронной
                                            почты.
                                            <p><strong>Пароль:</strong> В поле Пароль необходимо указать тот пароль,
                                                который Вы указали при регистрации.
                                            </p>
                                            <p><strong>Код с картинки:</strong> Введите в соответствующем поле цифры,
                                                изображенные на картинке. Это необходимо для защиты от роботов. </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF0000"><b>
                                    </b><img
                                            src="/public/home/blank.gif"
                                            width="290" height="26"></font></div>
                        </div>
                    </td>
                    <td align="top">
                        <p><font face="Arial, Helvetica, sans-serif" size="3"><b><br>
                                    <font size="4">Вход в Систему</font></b></font></p>
                        При входе в Ваш аккаунт, пожалуйста, введите ID Клиента, Пароль и Код с картинки<br>
                        <br>
                        Нет аккаунта? <a href="https://perfectmoney.is/signup.html">Регистрация</a><br>
                        <br>
                        <div class="error"><font face="Arial, Helvetica, sans-serif"></font></div>
                        <br>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <table cellpadding="0" cellspacing="2" border="0">
                                <tbody>
                                <tr>
                                    <td valign="top" width="123"><font face="Arial, Helvetica, sans-serif" size="2"><b>ID
                                                Клиента:</b></font></td>
                                    <td width="287"><font face="Arial, Helvetica, sans-serif" size="2">
                                            <input type="text" size="20" maxlength="20" value="" autocomplete="off" class="input">
                                            <input id="email" type="hidden" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="default@gmail.com" required autofocus autocomplete="off">
                                            &nbsp;&nbsp;
                                            <a href="#">Забыли?</a></font>
                                        <div class="error"><font face="Arial, Helvetica, sans-serif" size="2"></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td valign="top" width="123"><font face="Arial, Helvetica, sans-serif" size="2"><b>Пароль:</b></font>
                                    </td>
                                    <td width="287" nowrap=""><font face="Arial, Helvetica, sans-serif" size="2">
                                            {{--<input type="password" name="password" size="20" class="keyboardInput input" id="keyboardInputInitiator0">--}}
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                            <img src="/public/home/keyboard.png"
                                                    alt="Keyboard interface" class="keyboardInputInitiator"
                                                    title="Display graphical keyboard interface">&nbsp;&nbsp;<a
                                                    href="https://perfectmoney.is/restore.html">Потеряли пароль?</a>
                                        </font>
                                        <div class="error"><font face="Arial, Helvetica, sans-serif" size="2"></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td valign="top" width="123" height="23"><font face="Arial, Helvetica, sans-serif"
                                                                                   size="2">&nbsp;</font></td>
                                    <td width="57" height="23"><font face="Arial, Helvetica, sans-serif"
                                                                     size="2">&nbsp;</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="123"><font face="Arial, Helvetica, sans-serif" size="2"><b>Код
                                                с картинки:</b></font></td>
                                    <td width="57"><font face="Arial, Helvetica, sans-serif" size="2">
                                            <input type="text" name="turing" size="7" maxlength="10" autocomplete="off"
                                                   class="input">
                                        </font>
                                        <div class="error"><font face="Arial, Helvetica, sans-serif" size="2"></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="123"><font face="Arial, Helvetica, sans-serif" size="2"><a
                                                    style="text-decoration:none;cursor:pointer;" href="#">
                                                <img id="cpt_img" src="/public/home/capcha/image_{{rand(1, 10)}}.jpg"></a><br></font>
                                    </td>
                                    <td width="57">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="123" height="40">&nbsp;</td>
                                    <td width="57" height="40" valign="bottom">
                                        <input type="submit" value="Логин" name="submit" id="sbt" class="input submit">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <p>
                            </p></form>
                        <font face="Arial, Helvetica, sans-serif" size="3"><b><font size="4">Важно<br>
                                    <br>
                                </font></b></font>Всегда проверяйте наличие "Green Status Bar".<br>
                        <br>
                        <img src="/public/home/evis.gif">
                        <br>
                        <br>
                        Настоящий сайт Perfect Money всегда отображает зеленую адресную строку, которая подтверждает,
                        что контент, на сайте является законным и легальным.
                        <br>
                        <br>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="44" valign="middle"><img
                    src="/public/home/blank.gif"
                    width="44" height="26"></td>
    </tr>
    </tbody>
</table>
<!--
<table width="98%" style="margin: auto 1%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
    <tr>
        <td height="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>

                    <td width="98%" bgcolor="C61313"><img
                                src="/public/home/blank.gif"
                                width="1" height="1"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td height="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="1%"><img src="/public/home/left2.gif" width="19" height="6"></td>
          <td width="98%" bgcolor="C61313"><img src="/public/home/blank.gif" width="1" height="1"></td>
          <td width="1%" bgcolor="B01111" valign="middle"><img src="/public/home/right2.gif" width="19" height="6"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

@endsection
