<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];
}
