<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'action_id',
        'amount',
        'ip',
        'from_wallet',
        'to_wallet',
        'batch',
        'memo',
        'status',
        'wallet_type_id',
        'sort_date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->hasOne(LogType::class, 'id', 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function action()
    {
        return $this->hasOne(LogAction::class, 'id', 'action_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wallet_type()
    {
        return $this->hasOne(WalletType::class, 'id', 'wallet_type_id');
    }
}
