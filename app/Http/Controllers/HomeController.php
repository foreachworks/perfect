<?php

namespace App\Http\Controllers;

use App\Log;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use KubAT\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $last_login = Log::whereActionId(10)->orderBy('created_at', 'desc')->get();
        $old_time = isset($last_login[1]) ? Carbon::parse($last_login[1]->created_at)->format('Y-m-d H:i') : Carbon::parse($last_login[0]->created_at)->format('Y-m-d H:i');
        $wallets = Wallet::with('type')->whereStatus(1)->get();
        $logs = Log::with('type', 'action', 'wallet_type')->whereStatus(1)->orderBy('sort_date', 'DESC') ->offset(0)->limit(10)->get();
		$security = DB::table('users')->where('email', auth()->user()->email)->select('ip', 'sms', 'card', 'api')->first();

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

		return view('page.cabinet', [
			'wallets' => $wallets,
			'logs' => $logs,
			'old_time' => $old_time,
			'security' => $security,
			'banner' => $banner,
			'banner_spec' => $banner_spec
		]);
    }

    public function home()
    {
        return view('page.index2');
    }

    public function businessAccount()
    {
        $wallets = Wallet::all();

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        return view('page.business', compact('wallets', 'banner', 'banner_spec'));
    }

    public function history($paginate = false)
    {
        $wallets = Wallet::with('type')->whereStatus(1)->get();
        $logs = Log::with('type', 'action', 'wallet_type')->whereStatus(1)->orderBy('sort_date', 'DESC')->paginate($paginate ? $paginate : config('app.paginate_default'));
        $page = json_decode(response($logs)->content());
        $paginations = [
            'per_page' => $page->prev_page_url,
            'next_page' => $page->next_page_url,
        ];
		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

        if (!$paginate)
            $paginate =  config('app.paginate_default');

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        return view('page.history', compact('wallets', 'logs', 'paginations', 'paginate', 'banner', 'banner_spec'));
    }

    public function payment(Request $request)
    {
        $data = $request->except('_token');

        if (!isset($request->from_wallet)) {
            $wallet = Wallet::with('type')->find(session('data')['from_wallet']);
            $data['from_wallet'] = session('data')['from_wallet'];
        } else {
            $wallet = Wallet::with('type')->find($request->from_wallet);
        }

        $data['type_wallet'] = $wallet->type->title;
		$data['balance'] = $wallet->amount;
		$data['number'] = $wallet->title;

        \Session::put('data', $data);

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        $status = DB::table('wallet_status')->first();
		
		$wrn = DB::table('wrn_message')->first();

        return view('page.payment', [
            'data' => $data,
			'banner' => $banner,
			'status' => $status,
			'banner_spec' => $banner_spec,
			'wrn' => $wrn
        ]);
    }

    public function runPayment(Request $request)
    {
        $request = session('data');
        $wallet = Wallet::find($request['from_wallet']);
        $percent = $request['amount']*0.005;
        $wallet->update([
            'amount' => $wallet->amount - ($request['amount'] + $percent)
        ]);
        $next_batch = Log::max('batch');
        $default = 240718472;
        $log = Log::create([
            'type_id' => 2,
            'action_id' => 13,
            'amount' => $request['amount'],
            'ip' => '',
            'from_wallet' => $wallet->title,
            'to_wallet' => $request['to_wallet'],
            'batch' => empty($next_batch) ? $default : $next_batch + rand(500, 2743),
            'memo' => isset($request['memo']) ? $request['memo'] : '',
            'sort_date' => isset($request['sort_date']) ? $request['sort_date'] : Carbon::now()->subMinute(config('app.sub_minute'))->format('Y-m-d H:i:s'),
            'status' => 1,
            'wallet_type_id' => $wallet->type_id,
        ]);

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        return view('page.finish_payment', [
            'log' => $log->load('wallet_type'),
			'banner' => $banner,
			'banner_spec' => $banner_spec
        ]);
    }

    public function editPayment()
    {
        $data = session('data');

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        return view('page.edit_payment', [
            'data' => $data,
			'banner' => $banner,
			'banner_spec' => $banner_spec
        ]);
    }

    public function sendInternPayment($segment2)
    {
        if (!isset($segment2)) {
            $wallet = Wallet::with('type')->find($segment2);
            $data['from_wallet'] = $segment2;
        } else {
            $wallet = Wallet::with('type')->find($segment2);
            $data['from_wallet'] = $segment2;
        }

        $data['type_wallet'] = $wallet->type->title;
		$data['amount'] = '';
		$data['to_wallet'] = '';
		$data['balance'] = $wallet->amount;
		$data['number'] = $wallet->title;

        \Session::put('data', $data); 

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        return view('page.edit_payment', [
            'data' => $data,
			'banner' => $banner,
			'banner_spec' => $banner_spec
        ]);
    }

	public function internPayment()
	{
        $wallets = Wallet::with('type')->whereStatus(1)->orderBy('amount', 'desc')->get();

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

 		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

		return view('page.intern_payment', [
			'wallets' => $wallets,
			'banner' => $banner,
			'banner_spec' => $banner_spec
        ]);
	}
	
    public function security()
    {
        $wallets = Wallet::with('type')->whereStatus(1)->get();
		$security = DB::table('users')->where('email', auth()->user()->email)->select('ip', 'sms', 'card', 'api')->first();

		$rnd = rand(1, 4);
		$banner = DB::table('cabinet_banners')->where('id', $rnd)->select('img1', 'img2')->first();

 		$rnd_spec = rand(1, 6);
		$banner_spec = DB::table('spec_banners')->where('id', $rnd_spec)->select('img', 'title', 'text')->first();

        return view('page.security', compact('wallets', 'security', 'banner', 'banner_spec'));
    }
	
    public function change_security(Request $request)
    {
		$id = (int)$request->input('id');
		
		if($id == 1)
		{
			$query = DB::table('users')->where('email', auth()->user()->email)->select('ip')->first();
			
			($query->ip == 1) ? $ip = 0 : $ip = 1;

			DB::table('users')->where('email', auth()->user()->email)->update(['ip' => $ip]);
		}
		elseif($id == 2)
		{
			$query = DB::table('users')->where('email', auth()->user()->email)->select('sms')->first();
			
			($query->sms == 1) ? $sms = 0 : $sms = 1;

			DB::table('users')->where('email', auth()->user()->email)->update(['sms' => $sms]);
		}
		elseif($id == 3)
		{
			$query = DB::table('users')->where('email', auth()->user()->email)->select('card')->first();
			
			($query->card == 1) ? $card = 0 : $card = 1;

			DB::table('users')->where('email', auth()->user()->email)->update(['card' => $card]);
		}
		elseif($id == 4)
		{
			$query = DB::table('users')->where('email', auth()->user()->email)->select('api')->first();
			
			($query->api == 1) ? $api = 0 : $api = 1;

			DB::table('users')->where('email', auth()->user()->email)->update(['api' => $api]);
		}
		
        return response()->json(array('response' => TRUE));
	}
	
}
