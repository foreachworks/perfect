<?php

namespace App\Http\Controllers;

use App\User;
use App\Wallet;
use App\WalletType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $wallets = Wallet::all();

        return view('dashboard.index', compact('wallets'));
    }

    public function create()
    {
        $type = WalletType::pluck('title', 'id');

        return view('dashboard.wallet.form', compact('type'));
    }

    public function store(Request $request)
    {
        Wallet::create([
            'title' => $request->title,
            'amount' => $request->amount,
            'type_id' => $request->type,
            'status' => isset($request->status) ? 1 : 0,

        ]);

        return redirect()->route('dashboard');
    }

    public function update(Request $request, Wallet $wallet)
    {
        $wallet->update([
            'title' => $request->title,
            'amount' => $request->amount,
            'type_id' => $request->type,
            'status' => isset($request->status) ? 1 : 0,
        ]);

        return redirect()->route('dashboard');
    }

    public function edit(Wallet $wallet)
    {
        $type = WalletType::pluck('title', 'id');

        return view('dashboard.wallet.form', compact('type', 'wallet'));
    }

    public function destroy(Wallet $wallet)
    {
        $wallet->delete();

        return redirect()->route('dashboard');
    }

    public function account()
    {
        $user = User::first();
        $status = DB::table('wallet_status')->first();
		$wrn_status = DB::table('wrn_message')->first();

        return view('dashboard.info', compact('user', 'status', 'wrn_status'));
    }

    public function editInfo(User $user)
    {
        $status = DB::table('wallet_status')->first();

        return view('dashboard.edit', compact('user', 'status'));
    }

    public function updateInfo(Request $request)
    {
        $user = User::first();

        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'father_name' => $request->father_name,
            'trust_score' => $request->trust_score,
            'nickname_client' => $request->nickname_client,
            'business_account' => isset($request->business_account) ? $request->business_account : 0,
            'verified_account' => isset($request->verified_account) ? $request->verified_account : 0,
        ]);

        DB::table('wallet_status')->update([
            'business_account' => isset($request->business_account_w) ? $request->business_account_w : 0,
            'verified_account' => isset($request->verified_account_w) ? $request->verified_account_w : 0,
            'trust_score' => $request->trust_score_w,
        ]);

        return redirect()->route('account');
    }
    public function updateWrn(Request $request)
    {
        $wrn = DB::table('wrn_message')->first();
		
        (int)$wrn->wrn == 1 ? $new_status = 0 : $new_status = 1;

        DB::table('wrn_message')->update([
            'wrn' => $new_status
        ]);
		
		return response()->json(array('response'=>true));		
    }
}
