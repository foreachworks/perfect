<?php

namespace App\Http\Controllers;

use App\Log;
use App\LogAction;
use App\LogType;
use App\Wallet;
use App\WalletType;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index()
    {
        $logs = Log::with('wallet_type', 'action', 'type')->orderBy('sort_date', 'desc')->get();

        return view('dashboard.log.index', [
            'logs' => $logs
        ]);
    }

    public function create()
    {
        $type = LogType::pluck('title', 'id');
        $action = LogAction::pluck('title', 'id');
        $walletType = WalletType::pluck('title', 'id');
        $wallets = Wallet::get();

        return view('dashboard.log.form', [
            'walletType' => $walletType,
            'action' => $action,
            'type' => $type,
            'wallets' => $wallets
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        if (isset($data['sort_date'])) {
            if (!is_null($data['sort_date'])) {
                $time = $data['sort_time'];
                $data['sort_date'] = Carbon::parse($data['sort_date'])->format("Y-m-d $time");
            }
        }
        if ($data['ip'] == '') {
            $data['ip'] = '95.63.*.*';
        }

        Log::create($data);
        if (isset($data['to_wallet'])) {
            $wallet = Wallet::whereTitle($data['to_wallet'])->first();
            if (!is_null($wallet)) {
                $wallet->update([
                    'amount' => $wallet->amount + $data['amount']
                ]);
            }
        }

        return redirect()->route('log.index');
    }

    public function update(Request $request, Log $log)
    {
        $data = $request->except('_token', '_method');
        if (isset($data['sort_date'])) {
            if (!is_null($data['sort_date'])) {
                $time = $data['sort_time'];
                $data['sort_date'] = Carbon::parse($data['sort_date'])->format("Y-m-d $time");
            }
        }
        $log->update($data);

        return redirect()->route('log.index');
    }

    public function edit(Log $log)
    {
        $type = LogType::pluck('title', 'id');
        $action = LogAction::pluck('title', 'id');
        $walletType = WalletType::pluck('title', 'id');
        $wallets = Wallet::get();

        return view('dashboard.log.form', [
            'log' => $log,
            'walletType' => $walletType,
            'action' => $action,
            'type' => $type,
            'wallets' => $wallets
        ]);
    }

    public function destroy(Log $log)
    {
        $log->delete();

        return redirect()->route('log.index');
    }

    public function createRange()
    {
        $type = LogType::pluck('title', 'id');
        $action = LogAction::pluck('title', 'id');
        $walletType = WalletType::pluck('title', 'id');
        $wallets = Wallet::get();

        return view('dashboard.log.range', [
            'walletType' => $walletType,
            'action' => $action,
            'type' => $type,
            'wallets' => $wallets
        ]);
    }

    public function storeRange(Request $request)
    {
        $data = $request->except('_token');
        $batch = $data['batch'];

        $begin = Carbon::parse($data['form_date']);
        $end = Carbon::parse($data['to_date']);

        while ($begin <= $end){
            $time = rand($data['from_hour'], $data['to_hour']) . ':' . rand(0, 59);
            Log::create([
                'type_id' => 2,
                'action_id' => $data['action_id'],
                'amount' => $data['amount'],
                'ip' => '',
                'from_wallet' => $data['from_wallet'],
                'to_wallet' => $data['to_wallet'],
                'batch' => $batch,
                'memo' => $data['memo'],
                'sort_date' => Carbon::parse($begin)->format("Y-m-d $time"),
                'status' => 1,
                'wallet_type_id' => 2,
            ]);
            $begin = $begin->addDay();
            $batch += $data['step_batch'];
        }

        return redirect()->route('log.index');
    }

    /**
     * @param $min
     * @param $max
     * @return Carbon
     */
    public function randomDate($min, $max)
    {
        $year = rand(Carbon::parse($min)->format('y'), Carbon::parse($max)->format('y')) + 2000;
        $month = rand(Carbon::parse($min)->format('m'), Carbon::parse($max)->format('m'));
        $day = rand(Carbon::parse($min)->format('d'), Carbon::parse($max)->format('d'));
        $hour = rand(0, 23);
        $minute = rand(0, 59);

        return Carbon::create($year, $month, $day, $hour, $minute)->format('Y-m-d H:i:s');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear()
    {
        $lastLogin = Log::orderBy('created_at', 'desc')->where('action_id', 10)->first()->id;
        Log::whereNotIn('id', [$lastLogin])->delete();

        return back();
    }
}
